<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of functions used by the practise module.
 *
 * This contains functions that are called from within the practise module only
 * Functions that are also called by core Moodle are in {@link lib.php}
 * This script also loads the code in {@link questionlib.php} which holds
 * the module-indpendent code for handling questions and which in turn
 * initialises all the questiontype classes.
 *
 * @package    mod_practise
 * @copyright  1999 onwards Martin Dougiamas and others {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/practise/lib.php');
//require_once($CFG->dirroot . '/mod/practise/accessmanager.php');
//require_once($CFG->dirroot . '/mod/practise/accessmanager_form.php');
require_once($CFG->dirroot . '/mod/practise/renderer.php');
require_once($CFG->dirroot . '/mod/practise/attemptlib.php');
//require_once($CFG->libdir . '/completionlib.php');
//require_once($CFG->libdir . '/eventslib.php');
//require_once($CFG->libdir . '/filelib.php');
//require_once($CFG->libdir . '/questionlib.php');


/**
 * @var int We show the countdown timer if there is less than this amount of time left before the
 * the practise close date. (1 hour)
 */
define('practise_SHOW_TIME_BEFORE_DEADLINE', '3600');

/**
 * @var int If there are fewer than this many seconds left when the student submits
 * a page of the practise, then do not take them to the next page of the practise. Instead
 * close the practise immediately.
 */
define('practise_MIN_TIME_TO_CONTINUE', '2');

/**
 * @var int We show no image when user selects No image from dropdown menu in practise settings.
 */
define('practise_SHOWIMAGE_NONE', 0);

/**
 * @var int We show small image when user selects small image from dropdown menu in practise settings.
 */
define('practise_SHOWIMAGE_SMALL', 1);

/**
 * @var int We show Large image when user selects Large image from dropdown menu in practise settings.
 */
define('practise_SHOWIMAGE_LARGE', 2);


// Functions related to attempts ///////////////////////////////////////////////

/**
 * Creates an object to represent a new attempt at a practise
 *
 * Creates an attempt object to represent an attempt at the practise by the current
 * user starting at the current time. The ->id field is not set. The object is
 * NOT written to the database.
 *
 * @param object $practiseobj the practise object to create an attempt for.
 * @param int $attemptnumber the sequence number for the attempt.
 * @param object $lastattempt the previous attempt by this user, if any. Only needed
 *         if $attemptnumber > 1 and $practise->attemptonlast is true.
 * @param int $timenow the time the attempt was started at.
 * @param bool $ispreview whether this new attempt is a preview.
 * @param int $userid  the id of the user attempting this practise.
 *
 * @return object the newly created attempt object.
 */
function practise_create_attempt(practise $practiseobj, $attemptnumber, $lastattempt, $timenow, $ispreview = false, $userid = null) {
    global $USER;

    if ($userid === null) {
        $userid = $USER->id;
    }

    $practise = $practiseobj->get_practise();
    if ($practise->sumgrades < 0.000005 && $practise->grade > 0.000005) {
        throw new moodle_exception('cannotstartgradesmismatch', 'practise',
                new moodle_url('/mod/practise/view.php', array('q' => $practise->id)),
                    array('grade' => practise_format_grade($practise, $practise->grade)));
    }

    if ($attemptnumber == 1 || !$practise->attemptonlast) {
        // We are not building on last attempt so create a new attempt.
        $attempt = new stdClass();
        $attempt->practise = $practise->id;
        $attempt->userid = $userid;
        $attempt->preview = 0;
        $attempt->layout = '';
    } else {
        // Build on last attempt.
        if (empty($lastattempt)) {
            print_error('cannotfindprevattempt', 'practise');
        }
        $attempt = $lastattempt;
    }

    $attempt->attempt = $attemptnumber;
    $attempt->timestart = $timenow;
    $attempt->timefinish = 0;
    $attempt->timemodified = $timenow;
    $attempt->state = practise_attempt::IN_PROGRESS;
    $attempt->currentpage = 0;
    $attempt->sumgrades = null;

    // If this is a preview, mark it as such.
    if ($ispreview) {
        $attempt->preview = 1;
    }

    $timeclose = $practiseobj->get_access_manager($timenow)->get_end_time($attempt);
    if ($timeclose === false || $ispreview) {
        $attempt->timecheckstate = null;
    } else {
        $attempt->timecheckstate = $timeclose;
    }

    return $attempt;
}
/**
 * Start a normal, new, practise attempt.
 *
 * @param practise      $practiseobj            the practise object to start an attempt for.
 * @param question_usage_by_activity $quba
 * @param object    $attempt
 * @param integer   $attemptnumber      starting from 1
 * @param integer   $timenow            the attempt start time
 * @param array     $questionids        slot number => question id. Used for random questions, to force the choice
 *                                        of a particular actual question. Intended for testing purposes only.
 * @param array     $forcedvariantsbyslot slot number => variant. Used for questions with variants,
 *                                          to force the choice of a particular variant. Intended for testing
 *                                          purposes only.
 * @throws moodle_exception
 * @return object   modified attempt object
 */
function practise_start_new_attempt($practiseobj, $quba, $attempt, $attemptnumber, $timenow,
                                $questionids = array(), $forcedvariantsbyslot = array()) {

    // Usages for this user's previous practise attempts.
    $qubaids = new \mod_practise\question\qubaids_for_users_attempts(
            $practiseobj->get_practiseid(), $attempt->userid);

    // Fully load all the questions in this practise.
    $practiseobj->preload_questions();
    $practiseobj->load_questions();

    // First load all the non-random questions.
    $randomfound = false;
    $slot = 0;
    $questions = array();
    $maxmark = array();
    $page = array();
    foreach ($practiseobj->get_questions() as $questiondata) {
        $slot += 1;
        $maxmark[$slot] = $questiondata->maxmark;
        $page[$slot] = $questiondata->page;
        if ($questiondata->qtype == 'random') {
            $randomfound = true;
            continue;
        }
        if (!$practiseobj->get_practise()->shuffleanswers) {
            $questiondata->options->shuffleanswers = false;
        }
        $questions[$slot] = question_bank::make_question($questiondata);
    }

    // Then find a question to go in place of each random question.
    if ($randomfound) {
        $slot = 0;
        $usedquestionids = array();
        foreach ($questions as $question) {
            if (isset($usedquestions[$question->id])) {
                $usedquestionids[$question->id] += 1;
            } else {
                $usedquestionids[$question->id] = 1;
            }
        }
        $randomloader = new \core_question\bank\random_question_loader($qubaids, $usedquestionids);

        foreach ($practiseobj->get_questions() as $questiondata) {
            $slot += 1;
            if ($questiondata->qtype != 'random') {
                continue;
            }

            // Deal with fixed random choices for testing.
            if (isset($questionids[$quba->next_slot_number()])) {
                if ($randomloader->is_question_available($questiondata->category,
                        (bool) $questiondata->questiontext, $questionids[$quba->next_slot_number()])) {
                    $questions[$slot] = question_bank::load_question(
                            $questionids[$quba->next_slot_number()], $practiseobj->get_practise()->shuffleanswers);
                    continue;
                } else {
                    throw new coding_exception('Forced question id not available.');
                }
            }

            // Normal case, pick one at random.
            $questionid = $randomloader->get_next_question_id($questiondata->category,
                        (bool) $questiondata->questiontext);
            if ($questionid === null) {
                throw new moodle_exception('notenoughrandomquestions', 'practise',
                                           $practiseobj->view_url(), $questiondata);
            }

            $questions[$slot] = question_bank::load_question($questionid,
                    $practiseobj->get_practise()->shuffleanswers);
        }
    }

    // Finally add them all to the usage.
    ksort($questions);
    foreach ($questions as $slot => $question) {
        $newslot = $quba->add_question($question, $maxmark[$slot]);
        if ($newslot != $slot) {
            throw new coding_exception('Slot numbers have got confused.');
        }
    }

    // Start all the questions.
    $variantstrategy = new core_question\engine\variants\least_used_strategy($quba, $qubaids);

    if (!empty($forcedvariantsbyslot)) {
        $forcedvariantsbyseed = question_variant_forced_choices_selection_strategy::prepare_forced_choices_array(
            $forcedvariantsbyslot, $quba);
        $variantstrategy = new question_variant_forced_choices_selection_strategy(
            $forcedvariantsbyseed, $variantstrategy);
    }

    $quba->start_all_questions($variantstrategy, $timenow);

    // Work out the attempt layout.
    $sections = $practiseobj->get_sections();
    foreach ($sections as $i => $section) {
        if (isset($sections[$i + 1])) {
            $sections[$i]->lastslot = $sections[$i + 1]->firstslot - 1;
        } else {
            $sections[$i]->lastslot = count($questions);
        }
    }

    $layout = array();
    foreach ($sections as $section) {
        if ($section->shufflequestions) {
            $questionsinthissection = array();
            for ($slot = $section->firstslot; $slot <= $section->lastslot; $slot += 1) {
                $questionsinthissection[] = $slot;
            }
            shuffle($questionsinthissection);
            $questionsonthispage = 0;
            foreach ($questionsinthissection as $slot) {
                if ($questionsonthispage && $questionsonthispage == $practiseobj->get_practise()->questionsperpage) {
                    $layout[] = 0;
                    $questionsonthispage = 0;
                }
                $layout[] = $slot;
                $questionsonthispage += 1;
            }

        } else {
            $currentpage = $page[$section->firstslot];
            for ($slot = $section->firstslot; $slot <= $section->lastslot; $slot += 1) {
                if ($currentpage !== null && $page[$slot] != $currentpage) {
                    $layout[] = 0;
                }
                $layout[] = $slot;
                $currentpage = $page[$slot];
            }
        }

        // Each section ends with a page break.
        $layout[] = 0;
    }
    $attempt->layout = implode(',', $layout);

    return $attempt;
}

/**
 * Start a subsequent new attempt, in each attempt builds on last mode.
 *
 * @param question_usage_by_activity    $quba         this question usage
 * @param object                        $attempt      this attempt
 * @param object                        $lastattempt  last attempt
 * @return object                       modified attempt object
 *
 */
function practise_start_attempt_built_on_last($quba, $attempt, $lastattempt) {
    $oldquba = question_engine::load_questions_usage_by_activity($lastattempt->uniqueid);

    $oldnumberstonew = array();
    foreach ($oldquba->get_attempt_iterator() as $oldslot => $oldqa) {
        $newslot = $quba->add_question($oldqa->get_question(), $oldqa->get_max_mark());

        $quba->start_question_based_on($newslot, $oldqa);

        $oldnumberstonew[$oldslot] = $newslot;
    }

    // Update attempt layout.
    $newlayout = array();
    foreach (explode(',', $lastattempt->layout) as $oldslot) {
        if ($oldslot != 0) {
            $newlayout[] = $oldnumberstonew[$oldslot];
        } else {
            $newlayout[] = 0;
        }
    }
    $attempt->layout = implode(',', $newlayout);
    return $attempt;
}

/**
 * The save started question usage and practise attempt in db and log the started attempt.
 *
 * @param practise                       $practiseobj
 * @param question_usage_by_activity $quba
 * @param object                     $attempt
 * @return object                    attempt object with uniqueid and id set.
 */
function practise_attempt_save_started($practiseobj, $quba, $attempt) {
    global $DB;
    // Save the attempt in the database.
    question_engine::save_questions_usage_by_activity($quba);
    $attempt->uniqueid = $quba->get_id();
    $attempt->id = $DB->insert_record('practise_attempts', $attempt);

    // Params used by the events below.
    $params = array(
        'objectid' => $attempt->id,
        'relateduserid' => $attempt->userid,
        'courseid' => $practiseobj->get_courseid(),
        'context' => $practiseobj->get_context()
    );
    // Decide which event we are using.
    if ($attempt->preview) {
        $params['other'] = array(
            'practiseid' => $practiseobj->get_practiseid()
        );
        $event = \mod_practise\event\attempt_preview_started::create($params);
    } else {
        $event = \mod_practise\event\attempt_started::create($params);

    }

    // Trigger the event.
    $event->add_record_snapshot('practise', $practiseobj->get_practise());
    $event->add_record_snapshot('practise_attempts', $attempt);
    $event->trigger();

    return $attempt;
}

/**
 * Returns an unfinished attempt (if there is one) for the given
 * user on the given practise. This function does not return preview attempts.
 *
 * @param int $practiseid the id of the practise.
 * @param int $userid the id of the user.
 *
 * @return mixed the unfinished attempt if there is one, false if not.
 */
function practise_get_user_attempt_unfinished($practiseid, $userid) {
    $attempts = practise_get_user_attempts($practiseid, $userid, 'unfinished', true);
    if ($attempts) {
        return array_shift($attempts);
    } else {
        return false;
    }
}

/**
 * Delete a practise attempt.
 * @param mixed $attempt an integer attempt id or an attempt object
 *      (row of the practise_attempts table).
 * @param object $practise the practise object.
 */
function practise_delete_attempt($attempt, $practise) {
    global $DB;
    if (is_numeric($attempt)) {
        if (!$attempt = $DB->get_record('practise_attempts', array('id' => $attempt))) {
            return;
        }
    }

    if ($attempt->practise != $practise->id) {
        debugging("Trying to delete attempt $attempt->id which belongs to practise $attempt->practise " .
                "but was passed practise $practise->id.");
        return;
    }

    if (!isset($practise->cmid)) {
        $cm = get_coursemodule_from_instance('practise', $practise->id, $practise->course);
        $practise->cmid = $cm->id;
    }

    question_engine::delete_questions_usage_by_activity($attempt->uniqueid);
    $DB->delete_records('practise_attempts', array('id' => $attempt->id));

    // Log the deletion of the attempt if not a preview.
    if (!$attempt->preview) {
        $params = array(
            'objectid' => $attempt->id,
            'relateduserid' => $attempt->userid,
            'context' => context_module::instance($practise->cmid),
            'other' => array(
                'practiseid' => $practise->id
            )
        );
        $event = \mod_practise\event\attempt_deleted::create($params);
        $event->add_record_snapshot('practise_attempts', $attempt);
        $event->trigger();
    }

    // Search practise_attempts for other instances by this user.
    // If none, then delete record for this practise, this user from practise_grades
    // else recalculate best grade.
    $userid = $attempt->userid;
    if (!$DB->record_exists('practise_attempts', array('userid' => $userid, 'practise' => $practise->id))) {
        $DB->delete_records('practise_grades', array('userid' => $userid, 'practise' => $practise->id));
    } else {
        practise_save_best_grade($practise, $userid);
    }

    practise_update_grades($practise, $userid);
}

/**
 * Delete all the preview attempts at a practise, or possibly all the attempts belonging
 * to one user.
 * @param object $practise the practise object.
 * @param int $userid (optional) if given, only delete the previews belonging to this user.
 */
function practise_delete_previews($practise, $userid = null) {
    global $DB;
    $conditions = array('practise' => $practise->id, 'preview' => 1);
    if (!empty($userid)) {
        $conditions['userid'] = $userid;
    }
    $previewattempts = $DB->get_records('practise_attempts', $conditions);
    foreach ($previewattempts as $attempt) {
        practise_delete_attempt($attempt, $practise);
    }
}

/**
 * @param int $practiseid The practise id.
 * @return bool whether this practise has any (non-preview) attempts.
 */
function practise_has_attempts($practiseid) {
    global $DB;
    return $DB->record_exists('practise_attempts', array('practise' => $practiseid, 'preview' => 0));
}

// Functions to do with practise layout and pages //////////////////////////////////

/**
 * Repaginate the questions in a practise
 * @param int $practiseid the id of the practise to repaginate.
 * @param int $slotsperpage number of items to put on each page. 0 means unlimited.
 */
function practise_repaginate_questions($practiseid, $slotsperpage) {
    global $DB;
    $trans = $DB->start_delegated_transaction();

    $sections = $DB->get_records('practise_sections', array('practiseid' => $practiseid), 'firstslot ASC');
    $firstslots = array();
    foreach ($sections as $section) {
        if ((int)$section->firstslot === 1) {
            continue;
        }
        $firstslots[] = $section->firstslot;
    }

    $slots = $DB->get_records('practise_slots', array('practiseid' => $practiseid),
            'slot');
    $currentpage = 1;
    $slotsonthispage = 0;
    foreach ($slots as $slot) {
        if (($firstslots && in_array($slot->slot, $firstslots)) ||
            ($slotsonthispage && $slotsonthispage == $slotsperpage)) {
            $currentpage += 1;
            $slotsonthispage = 0;
        }
        if ($slot->page != $currentpage) {
            $DB->set_field('practise_slots', 'page', $currentpage, array('id' => $slot->id));
        }
        $slotsonthispage += 1;
    }

    $trans->allow_commit();
}

// Functions to do with practise grades ////////////////////////////////////////////

/**
 * Convert the raw grade stored in $attempt into a grade out of the maximum
 * grade for this practise.
 *
 * @param float $rawgrade the unadjusted grade, fof example $attempt->sumgrades
 * @param object $practise the practise object. Only the fields grade, sumgrades and decimalpoints are used.
 * @param bool|string $format whether to format the results for display
 *      or 'question' to format a question grade (different number of decimal places.
 * @return float|string the rescaled grade, or null/the lang string 'notyetgraded'
 *      if the $grade is null.
 */
function practise_rescale_grade($rawgrade, $practise, $format = true) {
    if (is_null($rawgrade)) {
        $grade = null;
    } else if ($practise->sumgrades >= 0.000005) {
        $grade = $rawgrade * $practise->grade / $practise->sumgrades;
    } else {
        $grade = 0;
    }
    if ($format === 'question') {
        $grade = practise_format_question_grade($practise, $grade);
    } else if ($format) {
        $grade = practise_format_grade($practise, $grade);
    }
    return $grade;
}

/**
 * Get the feedback object for this grade on this practise.
 *
 * @param float $grade a grade on this practise.
 * @param object $practise the practise settings.
 * @return false|stdClass the record object or false if there is not feedback for the given grade
 * @since  Moodle 3.1
 */
function practise_feedback_record_for_grade($grade, $practise) {
    global $DB;

    // With CBM etc, it is possible to get -ve grades, which would then not match
    // any feedback. Therefore, we replace -ve grades with 0.
    $grade = max($grade, 0);

    $feedback = $DB->get_record_select('practise_feedback',
            'practiseid = ? AND mingrade <= ? AND ? < maxgrade', array($practise->id, $grade, $grade));

    return $feedback;
}

/**
 * Get the feedback text that should be show to a student who
 * got this grade on this practise. The feedback is processed ready for diplay.
 *
 * @param float $grade a grade on this practise.
 * @param object $practise the practise settings.
 * @param object $context the practise context.
 * @return string the comment that corresponds to this grade (empty string if there is not one.
 */
function practise_feedback_for_grade($grade, $practise, $context) {

    if (is_null($grade)) {
        return '';
    }

    $feedback = practise_feedback_record_for_grade($grade, $practise);

    if (empty($feedback->feedbacktext)) {
        return '';
    }

    // Clean the text, ready for display.
    $formatoptions = new stdClass();
    $formatoptions->noclean = true;
    $feedbacktext = file_rewrite_pluginfile_urls($feedback->feedbacktext, 'pluginfile.php',
            $context->id, 'mod_practise', 'feedback', $feedback->id);
    $feedbacktext = format_text($feedbacktext, $feedback->feedbacktextformat, $formatoptions);

    return $feedbacktext;
}

/**
 * @param object $practise the practise database row.
 * @return bool Whether this practise has any non-blank feedback text.
 */
function practise_has_feedback($practise) {
    global $DB;
    static $cache = array();
    if (!array_key_exists($practise->id, $cache)) {
        $cache[$practise->id] = practise_has_grades($practise) &&
                $DB->record_exists_select('practise_feedback', "practiseid = ? AND " .
                    $DB->sql_isnotempty('practise_feedback', 'feedbacktext', false, true),
                array($practise->id));
    }
    return $cache[$practise->id];
}

/**
 * Update the sumgrades field of the practise. This needs to be called whenever
 * the grading structure of the practise is changed. For example if a question is
 * added or removed, or a question weight is changed.
 *
 * You should call {@link practise_delete_previews()} before you call this function.
 *
 * @param object $practise a practise.
 */
function practise_update_sumgrades($practise) {
    global $DB;

    $sql = 'UPDATE {practise}
            SET sumgrades = COALESCE((
                SELECT SUM(maxmark)
                FROM {practise_slots}
                WHERE practiseid = {practise}.id
            ), 0)
            WHERE id = ?';
    $DB->execute($sql, array($practise->id));
    $practise->sumgrades = $DB->get_field('practise', 'sumgrades', array('id' => $practise->id));

    if ($practise->sumgrades < 0.000005 && practise_has_attempts($practise->id)) {
        // If the practise has been attempted, and the sumgrades has been
        // set to 0, then we must also set the maximum possible grade to 0, or
        // we will get a divide by zero error.
        practise_set_grade(0, $practise);
    }
}

/**
 * Update the sumgrades field of the attempts at a practise.
 *
 * @param object $practise a practise.
 */
function practise_update_all_attempt_sumgrades($practise) {
    global $DB;
    $dm = new question_engine_data_mapper();
    $timenow = time();

    $sql = "UPDATE {practise_attempts}
            SET
                timemodified = :timenow,
                sumgrades = (
                    {$dm->sum_usage_marks_subquery('uniqueid')}
                )
            WHERE practise = :practiseid AND state = :finishedstate";
    $DB->execute($sql, array('timenow' => $timenow, 'practiseid' => $practise->id,
            'finishedstate' => practise_attempt::FINISHED));
}

/**
 * The practise grade is the maximum that student's results are marked out of. When it
 * changes, the corresponding data in practise_grades and practise_feedback needs to be
 * rescaled. After calling this function, you probably need to call
 * practise_update_all_attempt_sumgrades, practise_update_all_final_grades and
 * practise_update_grades.
 *
 * @param float $newgrade the new maximum grade for the practise.
 * @param object $practise the practise we are updating. Passed by reference so its
 *      grade field can be updated too.
 * @return bool indicating success or failure.
 */
function practise_set_grade($newgrade, $practise) {
    global $DB;
    // This is potentially expensive, so only do it if necessary.
    if (abs($practise->grade - $newgrade) < 1e-7) {
        // Nothing to do.
        return true;
    }

    $oldgrade = $practise->grade;
    $practise->grade = $newgrade;

    // Use a transaction, so that on those databases that support it, this is safer.
    $transaction = $DB->start_delegated_transaction();

    // Update the practise table.
    $DB->set_field('practise', 'grade', $newgrade, array('id' => $practise->instance));

    if ($oldgrade < 1) {
        // If the old grade was zero, we cannot rescale, we have to recompute.
        // We also recompute if the old grade was too small to avoid underflow problems.
        practise_update_all_final_grades($practise);

    } else {
        // We can rescale the grades efficiently.
        $timemodified = time();
        $DB->execute("
                UPDATE {practise_grades}
                SET grade = ? * grade, timemodified = ?
                WHERE practise = ?
        ", array($newgrade/$oldgrade, $timemodified, $practise->id));
    }

    if ($oldgrade > 1e-7) {
        // Update the practise_feedback table.
        $factor = $newgrade/$oldgrade;
        $DB->execute("
                UPDATE {practise_feedback}
                SET mingrade = ? * mingrade, maxgrade = ? * maxgrade
                WHERE practiseid = ?
        ", array($factor, $factor, $practise->id));
    }

    // Update grade item and send all grades to gradebook.
    practise_grade_item_update($practise);
    practise_update_grades($practise);

    $transaction->allow_commit();
    return true;
}

/**
 * Save the overall grade for a user at a practise in the practise_grades table
 *
 * @param object $practise The practise for which the best grade is to be calculated and then saved.
 * @param int $userid The userid to calculate the grade for. Defaults to the current user.
 * @param array $attempts The attempts of this user. Useful if you are
 * looping through many users. Attempts can be fetched in one master query to
 * avoid repeated querying.
 * @return bool Indicates success or failure.
 */
function practise_save_best_grade($practise, $userid = null, $attempts = array()) {
    global $DB, $OUTPUT, $USER;

    if (empty($userid)) {
        $userid = $USER->id;
    }

    if (!$attempts) {
        // Get all the attempts made by the user.
        $attempts = practise_get_user_attempts($practise->id, $userid);
    }

    // Calculate the best grade.
    $bestgrade = practise_calculate_best_grade($practise, $attempts);
    $bestgrade = practise_rescale_grade($bestgrade, $practise, false);

    // Save the best grade in the database.
    if (is_null($bestgrade)) {
        $DB->delete_records('practise_grades', array('practise' => $practise->id, 'userid' => $userid));

    } else if ($grade = $DB->get_record('practise_grades',
            array('practise' => $practise->id, 'userid' => $userid))) {
        $grade->grade = $bestgrade;
        $grade->timemodified = time();
        $DB->update_record('practise_grades', $grade);

    } else {
        $grade = new stdClass();
        $grade->practise = $practise->id;
        $grade->userid = $userid;
        $grade->grade = $bestgrade;
        $grade->timemodified = time();
        $DB->insert_record('practise_grades', $grade);
    }

    practise_update_grades($practise, $userid);
}

/**
 * Calculate the overall grade for a practise given a number of attempts by a particular user.
 *
 * @param object $practise    the practise settings object.
 * @param array $attempts an array of all the user's attempts at this practise in order.
 * @return float          the overall grade
 */
function practise_calculate_best_grade($practise, $attempts) {

    switch ($practise->grademethod) {

        case practise_ATTEMPTFIRST:
            $firstattempt = reset($attempts);
            return $firstattempt->sumgrades;

        case practise_ATTEMPTLAST:
            $lastattempt = end($attempts);
            return $lastattempt->sumgrades;

        case practise_GRADEAVERAGE:
            $sum = 0;
            $count = 0;
            foreach ($attempts as $attempt) {
                if (!is_null($attempt->sumgrades)) {
                    $sum += $attempt->sumgrades;
                    $count++;
                }
            }
            if ($count == 0) {
                return null;
            }
            return $sum / $count;

        case practise_GRADEHIGHEST:
        default:
            $max = null;
            foreach ($attempts as $attempt) {
                if ($attempt->sumgrades > $max) {
                    $max = $attempt->sumgrades;
                }
            }
            return $max;
    }
}

/**
 * Update the final grade at this practise for all students.
 *
 * This function is equivalent to calling practise_save_best_grade for all
 * users, but much more efficient.
 *
 * @param object $practise the practise settings.
 */
function practise_update_all_final_grades($practise) {
    global $DB;

    if (!$practise->sumgrades) {
        return;
    }

    $param = array('ipractiseid' => $practise->id, 'istatefinished' => practise_attempt::FINISHED);
    $firstlastattemptjoin = "JOIN (
            SELECT
                ipractisea.userid,
                MIN(attempt) AS firstattempt,
                MAX(attempt) AS lastattempt

            FROM {practise_attempts} ipractisea

            WHERE
                ipractisea.state = :istatefinished AND
                ipractisea.preview = 0 AND
                ipractisea.practise = :ipractiseid

            GROUP BY ipractisea.userid
        ) first_last_attempts ON first_last_attempts.userid = practisea.userid";

    switch ($practise->grademethod) {
        case practise_ATTEMPTFIRST:
            // Because of the where clause, there will only be one row, but we
            // must still use an aggregate function.
            $select = 'MAX(practisea.sumgrades)';
            $join = $firstlastattemptjoin;
            $where = 'practisea.attempt = first_last_attempts.firstattempt AND';
            break;

        case practise_ATTEMPTLAST:
            // Because of the where clause, there will only be one row, but we
            // must still use an aggregate function.
            $select = 'MAX(practisea.sumgrades)';
            $join = $firstlastattemptjoin;
            $where = 'practisea.attempt = first_last_attempts.lastattempt AND';
            break;

        case practise_GRADEAVERAGE:
            $select = 'AVG(practisea.sumgrades)';
            $join = '';
            $where = '';
            break;

        default:
        case practise_GRADEHIGHEST:
            $select = 'MAX(practisea.sumgrades)';
            $join = '';
            $where = '';
            break;
    }

    if ($practise->sumgrades >= 0.000005) {
        $finalgrade = $select . ' * ' . ($practise->grade / $practise->sumgrades);
    } else {
        $finalgrade = '0';
    }
    $param['practiseid'] = $practise->id;
    $param['practiseid2'] = $practise->id;
    $param['practiseid3'] = $practise->id;
    $param['practiseid4'] = $practise->id;
    $param['statefinished'] = practise_attempt::FINISHED;
    $param['statefinished2'] = practise_attempt::FINISHED;
    $finalgradesubquery = "
            SELECT practisea.userid, $finalgrade AS newgrade
            FROM {practise_attempts} practisea
            $join
            WHERE
                $where
                practisea.state = :statefinished AND
                practisea.preview = 0 AND
                practisea.practise = :practiseid3
            GROUP BY practisea.userid";

    $changedgrades = $DB->get_records_sql("
            SELECT users.userid, qg.id, qg.grade, newgrades.newgrade

            FROM (
                SELECT userid
                FROM {practise_grades} qg
                WHERE practise = :practiseid
            UNION
                SELECT DISTINCT userid
                FROM {practise_attempts} practisea2
                WHERE
                    practisea2.state = :statefinished2 AND
                    practisea2.preview = 0 AND
                    practisea2.practise = :practiseid2
            ) users

            LEFT JOIN {practise_grades} qg ON qg.userid = users.userid AND qg.practise = :practiseid4

            LEFT JOIN (
                $finalgradesubquery
            ) newgrades ON newgrades.userid = users.userid

            WHERE
                ABS(newgrades.newgrade - qg.grade) > 0.000005 OR
                ((newgrades.newgrade IS NULL OR qg.grade IS NULL) AND NOT
                          (newgrades.newgrade IS NULL AND qg.grade IS NULL))",
                // The mess on the previous line is detecting where the value is
                // NULL in one column, and NOT NULL in the other, but SQL does
                // not have an XOR operator, and MS SQL server can't cope with
                // (newgrades.newgrade IS NULL) <> (qg.grade IS NULL).
            $param);

    $timenow = time();
    $todelete = array();
    foreach ($changedgrades as $changedgrade) {

        if (is_null($changedgrade->newgrade)) {
            $todelete[] = $changedgrade->userid;

        } else if (is_null($changedgrade->grade)) {
            $toinsert = new stdClass();
            $toinsert->practise = $practise->id;
            $toinsert->userid = $changedgrade->userid;
            $toinsert->timemodified = $timenow;
            $toinsert->grade = $changedgrade->newgrade;
            $DB->insert_record('practise_grades', $toinsert);

        } else {
            $toupdate = new stdClass();
            $toupdate->id = $changedgrade->id;
            $toupdate->grade = $changedgrade->newgrade;
            $toupdate->timemodified = $timenow;
            $DB->update_record('practise_grades', $toupdate);
        }
    }

    if (!empty($todelete)) {
        list($test, $params) = $DB->get_in_or_equal($todelete);
        $DB->delete_records_select('practise_grades', 'practise = ? AND userid ' . $test,
                array_merge(array($practise->id), $params));
    }
}

/**
 * Efficiently update check state time on all open attempts
 *
 * @param array $conditions optional restrictions on which attempts to update
 *                    Allowed conditions:
 *                      courseid => (array|int) attempts in given course(s)
 *                      userid   => (array|int) attempts for given user(s)
 *                      practiseid   => (array|int) attempts in given practise(s)
 *                      groupid  => (array|int) practisezes with some override for given group(s)
 *
 */
function practise_update_open_attempts(array $conditions) {
    global $DB;

    foreach ($conditions as &$value) {
        if (!is_array($value)) {
            $value = array($value);
        }
    }

    $params = array();
    $wheres = array("practisea.state IN ('inprogress', 'overdue')");
    $iwheres = array("ipractisea.state IN ('inprogress', 'overdue')");

    if (isset($conditions['courseid'])) {
        list ($incond, $inparams) = $DB->get_in_or_equal($conditions['courseid'], SQL_PARAMS_NAMED, 'cid');
        $params = array_merge($params, $inparams);
        $wheres[] = "practisea.practise IN (SELECT q.id FROM {practise} q WHERE q.course $incond)";
        list ($incond, $inparams) = $DB->get_in_or_equal($conditions['courseid'], SQL_PARAMS_NAMED, 'icid');
        $params = array_merge($params, $inparams);
        $iwheres[] = "ipractisea.practise IN (SELECT q.id FROM {practise} q WHERE q.course $incond)";
    }

    if (isset($conditions['userid'])) {
        list ($incond, $inparams) = $DB->get_in_or_equal($conditions['userid'], SQL_PARAMS_NAMED, 'uid');
        $params = array_merge($params, $inparams);
        $wheres[] = "practisea.userid $incond";
        list ($incond, $inparams) = $DB->get_in_or_equal($conditions['userid'], SQL_PARAMS_NAMED, 'iuid');
        $params = array_merge($params, $inparams);
        $iwheres[] = "ipractisea.userid $incond";
    }

    if (isset($conditions['practiseid'])) {
        list ($incond, $inparams) = $DB->get_in_or_equal($conditions['practiseid'], SQL_PARAMS_NAMED, 'qid');
        $params = array_merge($params, $inparams);
        $wheres[] = "practisea.practise $incond";
        list ($incond, $inparams) = $DB->get_in_or_equal($conditions['practiseid'], SQL_PARAMS_NAMED, 'iqid');
        $params = array_merge($params, $inparams);
        $iwheres[] = "ipractisea.practise $incond";
    }

    if (isset($conditions['groupid'])) {
        list ($incond, $inparams) = $DB->get_in_or_equal($conditions['groupid'], SQL_PARAMS_NAMED, 'gid');
        $params = array_merge($params, $inparams);
        $wheres[] = "practisea.practise IN (SELECT qo.practise FROM {practise_overrides} qo WHERE qo.groupid $incond)";
        list ($incond, $inparams) = $DB->get_in_or_equal($conditions['groupid'], SQL_PARAMS_NAMED, 'igid');
        $params = array_merge($params, $inparams);
        $iwheres[] = "ipractisea.practise IN (SELECT qo.practise FROM {practise_overrides} qo WHERE qo.groupid $incond)";
    }

    // SQL to compute timeclose and timelimit for each attempt:
    $practiseausersql = practise_get_attempt_usertime_sql(
            implode("\n                AND ", $iwheres));

    // SQL to compute the new timecheckstate
    $timecheckstatesql = "
          CASE WHEN practiseauser.usertimelimit = 0 AND practiseauser.usertimeclose = 0 THEN NULL
               WHEN practiseauser.usertimelimit = 0 THEN practiseauser.usertimeclose
               WHEN practiseauser.usertimeclose = 0 THEN practisea.timestart + practiseauser.usertimelimit
               WHEN practisea.timestart + practiseauser.usertimelimit < practiseauser.usertimeclose THEN practisea.timestart + practiseauser.usertimelimit
               ELSE practiseauser.usertimeclose END +
          CASE WHEN practisea.state = 'overdue' THEN practise.graceperiod ELSE 0 END";

    // SQL to select which attempts to process
    $attemptselect = implode("\n                         AND ", $wheres);

   /*
    * Each database handles updates with inner joins differently:
    *  - mysql does not allow a FROM clause
    *  - postgres and mssql allow FROM but handle table aliases differently
    *  - oracle requires a subquery
    *
    * Different code for each database.
    */

    $dbfamily = $DB->get_dbfamily();
    if ($dbfamily == 'mysql') {
        $updatesql = "UPDATE {practise_attempts} practisea
                        JOIN {practise} practise ON practise.id = practisea.practise
                        JOIN ( $practiseausersql ) practiseauser ON practiseauser.id = practisea.id
                         SET practisea.timecheckstate = $timecheckstatesql
                       WHERE $attemptselect";
    } else if ($dbfamily == 'postgres') {
        $updatesql = "UPDATE {practise_attempts} practisea
                         SET timecheckstate = $timecheckstatesql
                        FROM {practise} practise, ( $practiseausersql ) practiseauser
                       WHERE practise.id = practisea.practise
                         AND practiseauser.id = practisea.id
                         AND $attemptselect";
    } else if ($dbfamily == 'mssql') {
        $updatesql = "UPDATE practisea
                         SET timecheckstate = $timecheckstatesql
                        FROM {practise_attempts} practisea
                        JOIN {practise} practise ON practise.id = practisea.practise
                        JOIN ( $practiseausersql ) practiseauser ON practiseauser.id = practisea.id
                       WHERE $attemptselect";
    } else {
        // oracle, sqlite and others
        $updatesql = "UPDATE {practise_attempts} practisea
                         SET timecheckstate = (
                           SELECT $timecheckstatesql
                             FROM {practise} practise, ( $practiseausersql ) practiseauser
                            WHERE practise.id = practisea.practise
                              AND practiseauser.id = practisea.id
                         )
                         WHERE $attemptselect";
    }

    $DB->execute($updatesql, $params);
}

/**
 * Returns SQL to compute timeclose and timelimit for every attempt, taking into account user and group overrides.
 *
 * @param string $redundantwhereclauses extra where clauses to add to the subquery
 *      for performance. These can use the table alias ipractisea for the practise attempts table.
 * @return string SQL select with columns attempt.id, usertimeclose, usertimelimit.
 */
function practise_get_attempt_usertime_sql($redundantwhereclauses = '') {
    if ($redundantwhereclauses) {
        $redundantwhereclauses = 'WHERE ' . $redundantwhereclauses;
    }
    // The multiple qgo JOINS are necessary because we want timeclose/timelimit = 0 (unlimited) to supercede
    // any other group override
    $practiseausersql = "
          SELECT ipractisea.id,
           COALESCE(MAX(quo.timeclose), MAX(qgo1.timeclose), MAX(qgo2.timeclose), ipractise.timeclose) AS usertimeclose,
           COALESCE(MAX(quo.timelimit), MAX(qgo3.timelimit), MAX(qgo4.timelimit), ipractise.timelimit) AS usertimelimit

           FROM {practise_attempts} ipractisea
           JOIN {practise} ipractise ON ipractise.id = ipractisea.practise
      LEFT JOIN {practise_overrides} quo ON quo.practise = ipractisea.practise AND quo.userid = ipractisea.userid
      LEFT JOIN {groups_members} gm ON gm.userid = ipractisea.userid
      LEFT JOIN {practise_overrides} qgo1 ON qgo1.practise = ipractisea.practise AND qgo1.groupid = gm.groupid AND qgo1.timeclose = 0
      LEFT JOIN {practise_overrides} qgo2 ON qgo2.practise = ipractisea.practise AND qgo2.groupid = gm.groupid AND qgo2.timeclose > 0
      LEFT JOIN {practise_overrides} qgo3 ON qgo3.practise = ipractisea.practise AND qgo3.groupid = gm.groupid AND qgo3.timelimit = 0
      LEFT JOIN {practise_overrides} qgo4 ON qgo4.practise = ipractisea.practise AND qgo4.groupid = gm.groupid AND qgo4.timelimit > 0
          $redundantwhereclauses
       GROUP BY ipractisea.id, ipractise.id, ipractise.timeclose, ipractise.timelimit";
    return $practiseausersql;
}

/**
 * Return the attempt with the best grade for a practise
 *
 * Which attempt is the best depends on $practise->grademethod. If the grade
 * method is GRADEAVERAGE then this function simply returns the last attempt.
 * @return object         The attempt with the best grade
 * @param object $practise    The practise for which the best grade is to be calculated
 * @param array $attempts An array of all the attempts of the user at the practise
 */
function practise_calculate_best_attempt($practise, $attempts) {

    switch ($practise->grademethod) {

        case practise_ATTEMPTFIRST:
            foreach ($attempts as $attempt) {
                return $attempt;
            }
            break;

        case practise_GRADEAVERAGE: // We need to do something with it.
        case practise_ATTEMPTLAST:
            foreach ($attempts as $attempt) {
                $final = $attempt;
            }
            return $final;

        default:
        case practise_GRADEHIGHEST:
            $max = -1;
            foreach ($attempts as $attempt) {
                if ($attempt->sumgrades > $max) {
                    $max = $attempt->sumgrades;
                    $maxattempt = $attempt;
                }
            }
            return $maxattempt;
    }
}

/**
 * @return array int => lang string the options for calculating the practise grade
 *      from the individual attempt grades.
 */
function practise_get_grading_options() {
    return array(
        practise_GRADEHIGHEST => get_string('gradehighest', 'practise'),
        practise_GRADEAVERAGE => get_string('gradeaverage', 'practise'),
        practise_ATTEMPTFIRST => get_string('attemptfirst', 'practise'),
        practise_ATTEMPTLAST  => get_string('attemptlast', 'practise')
    );
}

/**
 * @param int $option one of the values practise_GRADEHIGHEST, practise_GRADEAVERAGE,
 *      practise_ATTEMPTFIRST or practise_ATTEMPTLAST.
 * @return the lang string for that option.
 */
function practise_get_grading_option_name($option) {
    $strings = practise_get_grading_options();
    return $strings[$option];
}

/**
 * @return array string => lang string the options for handling overdue practise
 *      attempts.
 */
function practise_get_overdue_handling_options() {
    return array(
        'autosubmit'  => get_string('overduehandlingautosubmit', 'practise'),
        'graceperiod' => get_string('overduehandlinggraceperiod', 'practise'),
        'autoabandon' => get_string('overduehandlingautoabandon', 'practise'),
    );
}

/**
 * Get the choices for what size user picture to show.
 * @return array string => lang string the options for whether to display the user's picture.
 */
function practise_get_user_image_options() {
    return array(
        practise_SHOWIMAGE_NONE  => get_string('shownoimage', 'practise'),
        practise_SHOWIMAGE_SMALL => get_string('showsmallimage', 'practise'),
        practise_SHOWIMAGE_LARGE => get_string('showlargeimage', 'practise'),
    );
}

/**
 * Get the choices to offer for the 'Questions per page' option.
 * @return array int => string.
 */
function practise_questions_per_page_options() {
    $pageoptions = array();
    $pageoptions[0] = get_string('neverallononepage', 'practise');
    $pageoptions[1] = get_string('everyquestion', 'practise');
    for ($i = 2; $i <= practise_MAX_QPP_OPTION; ++$i) {
        $pageoptions[$i] = get_string('everynquestions', 'practise', $i);
    }
    return $pageoptions;
}

/**
 * Get the human-readable name for a practise attempt state.
 * @param string $state one of the state constants like {@link practise_attempt::IN_PROGRESS}.
 * @return string The lang string to describe that state.
 */
function practise_attempt_state_name($state) {
    switch ($state) {
        case practise_attempt::IN_PROGRESS:
            return get_string('stateinprogress', 'practise');
        case practise_attempt::OVERDUE:
            return get_string('stateoverdue', 'practise');
        case practise_attempt::FINISHED:
            return get_string('statefinished', 'practise');
        case practise_attempt::ABANDONED:
            return get_string('stateabandoned', 'practise');
        default:
            throw new coding_exception('Unknown practise attempt state.');
    }
}

// Other practise functions ////////////////////////////////////////////////////////

/**
 * @param object $practise the practise.
 * @param int $cmid the course_module object for this practise.
 * @param object $question the question.
 * @param string $returnurl url to return to after action is done.
 * @param int $variant which question variant to preview (optional).
 * @return string html for a number of icons linked to action pages for a
 * question - preview and edit / view icons depending on user capabilities.
 */
function practise_question_action_icons($practise, $cmid, $question, $returnurl, $variant = null) {
    $html = practise_question_preview_button($practise, $question, false, $variant) . ' ' .
            practise_question_edit_button($cmid, $question, $returnurl);
    return $html;
}

/**
 * @param int $cmid the course_module.id for this practise.
 * @param object $question the question.
 * @param string $returnurl url to return to after action is done.
 * @param string $contentbeforeicon some HTML content to be added inside the link, before the icon.
 * @return the HTML for an edit icon, view icon, or nothing for a question
 *      (depending on permissions).
 */
function practise_question_edit_button($cmid, $question, $returnurl, $contentaftericon = '') {
    global $CFG, $OUTPUT;

    // Minor efficiency saving. Only get strings once, even if there are a lot of icons on one page.
    static $stredit = null;
    static $strview = null;
    if ($stredit === null) {
        $stredit = get_string('edit');
        $strview = get_string('view');
    }

    // What sort of icon should we show?
    $action = '';
    if (!empty($question->id) &&
            (question_has_capability_on($question, 'edit', $question->category) ||
                    question_has_capability_on($question, 'move', $question->category))) {
        $action = $stredit;
        $icon = '/t/edit';
    } else if (!empty($question->id) &&
            question_has_capability_on($question, 'view', $question->category)) {
        $action = $strview;
        $icon = '/i/info';
    }

    // Build the icon.
    if ($action) {
        if ($returnurl instanceof moodle_url) {
            $returnurl = $returnurl->out_as_local_url(false);
        }
        $questionparams = array('returnurl' => $returnurl, 'cmid' => $cmid, 'id' => $question->id);
        $questionurl = new moodle_url("$CFG->wwwroot/question/question.php", $questionparams);
        return '<a title="' . $action . '" href="' . $questionurl->out() . '" class="questioneditbutton"><img src="' .
                $OUTPUT->pix_url($icon) . '" alt="' . $action . '" />' . $contentaftericon .
                '</a>';
    } else if ($contentaftericon) {
        return '<span class="questioneditbutton">' . $contentaftericon . '</span>';
    } else {
        return '';
    }
}

/**
 * @param object $practise the practise settings
 * @param object $question the question
 * @param int $variant which question variant to preview (optional).
 * @return moodle_url to preview this question with the options from this practise.
 */
function practise_question_preview_url($practise, $question, $variant = null) {
    // Get the appropriate display options.
    $displayoptions = mod_practise_display_options::make_from_practise($practise,
            mod_practise_display_options::DURING);

    $maxmark = null;
    if (isset($question->maxmark)) {
        $maxmark = $question->maxmark;
    }

    // Work out the correcte preview URL.
    return question_preview_url($question->id, $practise->preferredbehaviour,
            $maxmark, $displayoptions, $variant);
}

/**
 * @param object $practise the practise settings
 * @param object $question the question
 * @param bool $label if true, show the preview question label after the icon
 * @param int $variant which question variant to preview (optional).
 * @return the HTML for a preview question icon.
 */
function practise_question_preview_button($practise, $question, $label = false, $variant = null) {
    global $PAGE;
    if (!question_has_capability_on($question, 'use', $question->category)) {
        return '';
    }

    return $PAGE->get_renderer('mod_practise', 'edit')->question_preview_icon($practise, $question, $label, $variant);
}

/**
 * @param object $attempt the attempt.
 * @param object $context the practise context.
 * @return int whether flags should be shown/editable to the current user for this attempt.
 */
function practise_get_flag_option($attempt, $context) {
    global $USER;
    if (!has_capability('moodle/question:flag', $context)) {
        return question_display_options::HIDDEN;
    } else if ($attempt->userid == $USER->id) {
        return question_display_options::EDITABLE;
    } else {
        return question_display_options::VISIBLE;
    }
}

/**
 * Work out what state this practise attempt is in - in the sense used by
 * practise_get_review_options, not in the sense of $attempt->state.
 * @param object $practise the practise settings
 * @param object $attempt the practise_attempt database row.
 * @return int one of the mod_practise_display_options::DURING,
 *      IMMEDIATELY_AFTER, LATER_WHILE_OPEN or AFTER_CLOSE constants.
 */
function practise_attempt_state($practise, $attempt) {
    if ($attempt->state == practise_attempt::IN_PROGRESS) {
        return mod_practise_display_options::DURING;
    } else if ($practise->timeclose && time() >= $practise->timeclose) {
        return mod_practise_display_options::AFTER_CLOSE;
    } else if (time() < $attempt->timefinish + 120) {
        return mod_practise_display_options::IMMEDIATELY_AFTER;
    } else {
        return mod_practise_display_options::LATER_WHILE_OPEN;
    }
}

/**
 * The the appropraite mod_practise_display_options object for this attempt at this
 * practise right now.
 *
 * @param object $practise the practise instance.
 * @param object $attempt the attempt in question.
 * @param $context the practise context.
 *
 * @return mod_practise_display_options
 */
function practise_get_review_options($practise, $attempt, $context) {
    $options = mod_practise_display_options::make_from_practise($practise, practise_attempt_state($practise, $attempt));

    $options->readonly = true;
    $options->flags = practise_get_flag_option($attempt, $context);
    if (!empty($attempt->id)) {
        $options->questionreviewlink = new moodle_url('/mod/practise/reviewquestion.php',
                array('attempt' => $attempt->id));
    }

    // Show a link to the comment box only for closed attempts.
    if (!empty($attempt->id) && $attempt->state == practise_attempt::FINISHED && !$attempt->preview &&
            !is_null($context) && has_capability('mod/practise:grade', $context)) {
        $options->manualcomment = question_display_options::VISIBLE;
        $options->manualcommentlink = new moodle_url('/mod/practise/comment.php',
                array('attempt' => $attempt->id));
    }

    if (!is_null($context) && !$attempt->preview &&
            has_capability('mod/practise:viewreports', $context) &&
            has_capability('moodle/grade:viewhidden', $context)) {
        // People who can see reports and hidden grades should be shown everything,
        // except during preview when teachers want to see what students see.
        $options->attempt = question_display_options::VISIBLE;
        $options->correctness = question_display_options::VISIBLE;
        $options->marks = question_display_options::MARK_AND_MAX;
        $options->feedback = question_display_options::VISIBLE;
        $options->numpartscorrect = question_display_options::VISIBLE;
        $options->manualcomment = question_display_options::VISIBLE;
        $options->generalfeedback = question_display_options::VISIBLE;
        $options->rightanswer = question_display_options::VISIBLE;
        $options->overallfeedback = question_display_options::VISIBLE;
        $options->history = question_display_options::VISIBLE;

    }

    return $options;
}

/**
 * Combines the review options from a number of different practise attempts.
 * Returns an array of two ojects, so the suggested way of calling this
 * funciton is:
 * list($someoptions, $alloptions) = practise_get_combined_reviewoptions(...)
 *
 * @param object $practise the practise instance.
 * @param array $attempts an array of attempt objects.
 *
 * @return array of two options objects, one showing which options are true for
 *          at least one of the attempts, the other showing which options are true
 *          for all attempts.
 */
function practise_get_combined_reviewoptions($practise, $attempts) {
    $fields = array('feedback', 'generalfeedback', 'rightanswer', 'overallfeedback');
    $someoptions = new stdClass();
    $alloptions = new stdClass();
    foreach ($fields as $field) {
        $someoptions->$field = false;
        $alloptions->$field = true;
    }
    $someoptions->marks = question_display_options::HIDDEN;
    $alloptions->marks = question_display_options::MARK_AND_MAX;

    // This shouldn't happen, but we need to prevent reveal information.
    if (empty($attempts)) {
        return array($someoptions, $someoptions);
    }

    foreach ($attempts as $attempt) {
        $attemptoptions = mod_practise_display_options::make_from_practise($practise,
                practise_attempt_state($practise, $attempt));
        foreach ($fields as $field) {
            $someoptions->$field = $someoptions->$field || $attemptoptions->$field;
            $alloptions->$field = $alloptions->$field && $attemptoptions->$field;
        }
        $someoptions->marks = max($someoptions->marks, $attemptoptions->marks);
        $alloptions->marks = min($alloptions->marks, $attemptoptions->marks);
    }
    return array($someoptions, $alloptions);
}

// Functions for sending notification messages /////////////////////////////////

/**
 * Sends a confirmation message to the student confirming that the attempt was processed.
 *
 * @param object $a lots of useful information that can be used in the message
 *      subject and body.
 *
 * @return int|false as for {@link message_send()}.
 */
function practise_send_confirmation($recipient, $a) {

    // Add information about the recipient to $a.
    // Don't do idnumber. we want idnumber to be the submitter's idnumber.
    $a->username     = fullname($recipient);
    $a->userusername = $recipient->username;

    // Prepare the message.
    $eventdata = new stdClass();
    $eventdata->component         = 'mod_practise';
    $eventdata->name              = 'confirmation';
    $eventdata->notification      = 1;

    $eventdata->userfrom          = core_user::get_noreply_user();
    $eventdata->userto            = $recipient;
    $eventdata->subject           = get_string('emailconfirmsubject', 'practise', $a);
    $eventdata->fullmessage       = get_string('emailconfirmbody', 'practise', $a);
    $eventdata->fullmessageformat = FORMAT_PLAIN;
    $eventdata->fullmessagehtml   = '';

    $eventdata->smallmessage      = get_string('emailconfirmsmall', 'practise', $a);
    $eventdata->contexturl        = $a->practiseurl;
    $eventdata->contexturlname    = $a->practisename;

    // ... and send it.
    return message_send($eventdata);
}

/**
 * Sends notification messages to the interested parties that assign the role capability
 *
 * @param object $recipient user object of the intended recipient
 * @param object $a associative array of replaceable fields for the templates
 *
 * @return int|false as for {@link message_send()}.
 */
function practise_send_notification($recipient, $submitter, $a) {

    // Recipient info for template.
    $a->useridnumber = $recipient->idnumber;
    $a->username     = fullname($recipient);
    $a->userusername = $recipient->username;

    // Prepare the message.
    $eventdata = new stdClass();
    $eventdata->component         = 'mod_practise';
    $eventdata->name              = 'submission';
    $eventdata->notification      = 1;

    $eventdata->userfrom          = $submitter;
    $eventdata->userto            = $recipient;
    $eventdata->subject           = get_string('emailnotifysubject', 'practise', $a);
    $eventdata->fullmessage       = get_string('emailnotifybody', 'practise', $a);
    $eventdata->fullmessageformat = FORMAT_PLAIN;
    $eventdata->fullmessagehtml   = '';

    $eventdata->smallmessage      = get_string('emailnotifysmall', 'practise', $a);
    $eventdata->contexturl        = $a->practisereviewurl;
    $eventdata->contexturlname    = $a->practisename;

    // ... and send it.
    return message_send($eventdata);
}

/**
 * Send all the requried messages when a practise attempt is submitted.
 *
 * @param object $course the course
 * @param object $practise the practise
 * @param object $attempt this attempt just finished
 * @param object $context the practise context
 * @param object $cm the coursemodule for this practise
 *
 * @return bool true if all necessary messages were sent successfully, else false.
 */
function practise_send_notification_messages($course, $practise, $attempt, $context, $cm) {
    global $CFG, $DB;

    // Do nothing if required objects not present.
    if (empty($course) or empty($practise) or empty($attempt) or empty($context)) {
        throw new coding_exception('$course, $practise, $attempt, $context and $cm must all be set.');
    }

    $submitter = $DB->get_record('user', array('id' => $attempt->userid), '*', MUST_EXIST);

    // Check for confirmation required.
    $sendconfirm = false;
    $notifyexcludeusers = '';
    if (has_capability('mod/practise:emailconfirmsubmission', $context, $submitter, false)) {
        $notifyexcludeusers = $submitter->id;
        $sendconfirm = true;
    }

    // Check for notifications required.
    $notifyfields = 'u.id, u.username, u.idnumber, u.email, u.emailstop, u.lang,
            u.timezone, u.mailformat, u.maildisplay, u.auth, u.suspended, u.deleted, ';
    $notifyfields .= get_all_user_name_fields(true, 'u');
    $groups = groups_get_all_groups($course->id, $submitter->id, $cm->groupingid);
    if (is_array($groups) && count($groups) > 0) {
        $groups = array_keys($groups);
    } else if (groups_get_activity_groupmode($cm, $course) != NOGROUPS) {
        // If the user is not in a group, and the practise is set to group mode,
        // then set $groups to a non-existant id so that only users with
        // 'moodle/site:accessallgroups' get notified.
        $groups = -1;
    } else {
        $groups = '';
    }
    $userstonotify = get_users_by_capability($context, 'mod/practise:emailnotifysubmission',
            $notifyfields, '', '', '', $groups, $notifyexcludeusers, false, false, true);

    if (empty($userstonotify) && !$sendconfirm) {
        return true; // Nothing to do.
    }

    $a = new stdClass();
    // Course info.
    $a->coursename      = $course->fullname;
    $a->courseshortname = $course->shortname;
    // practise info.
    $a->practisename        = $practise->name;
    $a->practisereporturl   = $CFG->wwwroot . '/mod/practise/report.php?id=' . $cm->id;
    $a->practisereportlink  = '<a href="' . $a->practisereporturl . '">' .
            format_string($practise->name) . ' report</a>';
    $a->practiseurl         = $CFG->wwwroot . '/mod/practise/view.php?id=' . $cm->id;
    $a->practiselink        = '<a href="' . $a->practiseurl . '">' . format_string($practise->name) . '</a>';
    // Attempt info.
    $a->submissiontime  = userdate($attempt->timefinish);
    $a->timetaken       = format_time($attempt->timefinish - $attempt->timestart);
    $a->practisereviewurl   = $CFG->wwwroot . '/mod/practise/review.php?attempt=' . $attempt->id;
    $a->practisereviewlink  = '<a href="' . $a->practisereviewurl . '">' .
            format_string($practise->name) . ' review</a>';
    // Student who sat the practise info.
    $a->studentidnumber = $submitter->idnumber;
    $a->studentname     = fullname($submitter);
    $a->studentusername = $submitter->username;

    $allok = true;

    // Send notifications if required.
    if (!empty($userstonotify)) {
        foreach ($userstonotify as $recipient) {
            $allok = $allok && practise_send_notification($recipient, $submitter, $a);
        }
    }

    // Send confirmation if required. We send the student confirmation last, so
    // that if message sending is being intermittently buggy, which means we send
    // some but not all messages, and then try again later, then teachers may get
    // duplicate messages, but the student will always get exactly one.
    if ($sendconfirm) {
        $allok = $allok && practise_send_confirmation($submitter, $a);
    }

    return $allok;
}

/**
 * Send the notification message when a practise attempt becomes overdue.
 *
 * @param practise_attempt $attemptobj all the data about the practise attempt.
 */
function practise_send_overdue_message($attemptobj) {
    global $CFG, $DB;

    $submitter = $DB->get_record('user', array('id' => $attemptobj->get_userid()), '*', MUST_EXIST);

    if (!$attemptobj->has_capability('mod/practise:emailwarnoverdue', $submitter->id, false)) {
        return; // Message not required.
    }

    if (!$attemptobj->has_response_to_at_least_one_graded_question()) {
        return; // Message not required.
    }

    // Prepare lots of useful information that admins might want to include in
    // the email message.
    $practisename = format_string($attemptobj->get_practise_name());

    $deadlines = array();
    if ($attemptobj->get_practise()->timelimit) {
        $deadlines[] = $attemptobj->get_attempt()->timestart + $attemptobj->get_practise()->timelimit;
    }
    if ($attemptobj->get_practise()->timeclose) {
        $deadlines[] = $attemptobj->get_practise()->timeclose;
    }
    $duedate = min($deadlines);
    $graceend = $duedate + $attemptobj->get_practise()->graceperiod;

    $a = new stdClass();
    // Course info.
    $a->coursename         = format_string($attemptobj->get_course()->fullname);
    $a->courseshortname    = format_string($attemptobj->get_course()->shortname);
    // practise info.
    $a->practisename           = $practisename;
    $a->practiseurl            = $attemptobj->view_url();
    $a->practiselink           = '<a href="' . $a->practiseurl . '">' . $practisename . '</a>';
    // Attempt info.
    $a->attemptduedate     = userdate($duedate);
    $a->attemptgraceend    = userdate($graceend);
    $a->attemptsummaryurl  = $attemptobj->summary_url()->out(false);
    $a->attemptsummarylink = '<a href="' . $a->attemptsummaryurl . '">' . $practisename . ' review</a>';
    // Student's info.
    $a->studentidnumber    = $submitter->idnumber;
    $a->studentname        = fullname($submitter);
    $a->studentusername    = $submitter->username;

    // Prepare the message.
    $eventdata = new stdClass();
    $eventdata->component         = 'mod_practise';
    $eventdata->name              = 'attempt_overdue';
    $eventdata->notification      = 1;

    $eventdata->userfrom          = core_user::get_noreply_user();
    $eventdata->userto            = $submitter;
    $eventdata->subject           = get_string('emailoverduesubject', 'practise', $a);
    $eventdata->fullmessage       = get_string('emailoverduebody', 'practise', $a);
    $eventdata->fullmessageformat = FORMAT_PLAIN;
    $eventdata->fullmessagehtml   = '';

    $eventdata->smallmessage      = get_string('emailoverduesmall', 'practise', $a);
    $eventdata->contexturl        = $a->practiseurl;
    $eventdata->contexturlname    = $a->practisename;

    // Send the message.
    return message_send($eventdata);
}

/**
 * Handle the practise_attempt_submitted event.
 *
 * This sends the confirmation and notification messages, if required.
 *
 * @param object $event the event object.
 */
function practise_attempt_submitted_handler($event) {
    global $DB;

    $course  = $DB->get_record('course', array('id' => $event->courseid));
    $attempt = $event->get_record_snapshot('practise_attempts', $event->objectid);
    $practise    = $event->get_record_snapshot('practise', $attempt->practise);
    $cm      = get_coursemodule_from_id('practise', $event->get_context()->instanceid, $event->courseid);

    if (!($course && $practise && $cm && $attempt)) {
        // Something has been deleted since the event was raised. Therefore, the
        // event is no longer relevant.
        return true;
    }

    // Update completion state.
    $completion = new completion_info($course);
    if ($completion->is_enabled($cm) && ($practise->completionattemptsexhausted || $practise->completionpass)) {
        $completion->update_state($cm, COMPLETION_COMPLETE, $event->userid);
    }
    return practise_send_notification_messages($course, $practise, $attempt,
            context_module::instance($cm->id), $cm);
}

/**
 * Handle groups_member_added event
 *
 * @param object $event the event object.
 * @deprecated since 2.6, see {@link \mod_practise\group_observers::group_member_added()}.
 */
function practise_groups_member_added_handler($event) {
    debugging('practise_groups_member_added_handler() is deprecated, please use ' .
        '\mod_practise\group_observers::group_member_added() instead.', DEBUG_DEVELOPER);
    practise_update_open_attempts(array('userid'=>$event->userid, 'groupid'=>$event->groupid));
}

/**
 * Handle groups_member_removed event
 *
 * @param object $event the event object.
 * @deprecated since 2.6, see {@link \mod_practise\group_observers::group_member_removed()}.
 */
function practise_groups_member_removed_handler($event) {
    debugging('practise_groups_member_removed_handler() is deprecated, please use ' .
        '\mod_practise\group_observers::group_member_removed() instead.', DEBUG_DEVELOPER);
    practise_update_open_attempts(array('userid'=>$event->userid, 'groupid'=>$event->groupid));
}

/**
 * Handle groups_group_deleted event
 *
 * @param object $event the event object.
 * @deprecated since 2.6, see {@link \mod_practise\group_observers::group_deleted()}.
 */
function practise_groups_group_deleted_handler($event) {
    global $DB;
    debugging('practise_groups_group_deleted_handler() is deprecated, please use ' .
        '\mod_practise\group_observers::group_deleted() instead.', DEBUG_DEVELOPER);
    practise_process_group_deleted_in_course($event->courseid);
}

/**
 * Logic to happen when a/some group(s) has/have been deleted in a course.
 *
 * @param int $courseid The course ID.
 * @return void
 */
function practise_process_group_deleted_in_course($courseid) {
    global $DB;

    // It would be nice if we got the groupid that was deleted.
    // Instead, we just update all practisezes with orphaned group overrides.
    $sql = "SELECT o.id, o.practise
              FROM {practise_overrides} o
              JOIN {practise} practise ON practise.id = o.practise
         LEFT JOIN {groups} grp ON grp.id = o.groupid
             WHERE practise.course = :courseid
               AND o.groupid IS NOT NULL
               AND grp.id IS NULL";
    $params = array('courseid' => $courseid);
    $records = $DB->get_records_sql_menu($sql, $params);
    if (!$records) {
        return; // Nothing to do.
    }
    $DB->delete_records_list('practise_overrides', 'id', array_keys($records));
    practise_update_open_attempts(array('practiseid' => array_unique(array_values($records))));
}

/**
 * Handle groups_members_removed event
 *
 * @param object $event the event object.
 * @deprecated since 2.6, see {@link \mod_practise\group_observers::group_member_removed()}.
 */
function practise_groups_members_removed_handler($event) {
    debugging('practise_groups_members_removed_handler() is deprecated, please use ' .
        '\mod_practise\group_observers::group_member_removed() instead.', DEBUG_DEVELOPER);
    if ($event->userid == 0) {
        practise_update_open_attempts(array('courseid'=>$event->courseid));
    } else {
        practise_update_open_attempts(array('courseid'=>$event->courseid, 'userid'=>$event->userid));
    }
}

/**
 * Get the information about the standard practise JavaScript module.
 * @return array a standard jsmodule structure.
 */
function practise_get_js_module() {
    global $PAGE;

    return array(
        'name' => 'mod_practise',
        'fullpath' => '/mod/practise/module.js',
        'requires' => array('base', 'dom', 'event-delegate', 'event-key',
                'core_question_engine', 'moodle-core-formchangechecker'),
        'strings' => array(
            array('cancel', 'moodle'),
            array('flagged', 'question'),
            array('functiondisabledbysecuremode', 'practise'),
            array('startattempt', 'practise'),
            array('timesup', 'practise'),
            array('changesmadereallygoaway', 'moodle'),
        ),
    );
}


/**
 * An extension of question_display_options that includes the extra options used
 * by the practise.
 *
 * @copyright  2010 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_practise_display_options extends question_display_options {
    /**#@+
     * @var integer bits used to indicate various times in relation to a
     * practise attempt.
     */
    const DURING =            0x10000;
    const IMMEDIATELY_AFTER = 0x01000;
    const LATER_WHILE_OPEN =  0x00100;
    const AFTER_CLOSE =       0x00010;
    /**#@-*/

    /**
     * @var boolean if this is false, then the student is not allowed to review
     * anything about the attempt.
     */
    public $attempt = true;

    /**
     * @var boolean if this is false, then the student is not allowed to review
     * anything about the attempt.
     */
    public $overallfeedback = self::VISIBLE;

    /**
     * Set up the various options from the practise settings, and a time constant.
     * @param object $practise the practise settings.
     * @param int $one of the {@link DURING}, {@link IMMEDIATELY_AFTER},
     * {@link LATER_WHILE_OPEN} or {@link AFTER_CLOSE} constants.
     * @return mod_practise_display_options set up appropriately.
     */
    public static function make_from_practise($practise, $when) {
        $options = new self();

        $options->attempt = self::extract($practise->reviewattempt, $when, true, false);
        $options->correctness = self::extract($practise->reviewcorrectness, $when);
        $options->marks = self::extract($practise->reviewmarks, $when,
                self::MARK_AND_MAX, self::MAX_ONLY);
        $options->feedback = self::extract($practise->reviewspecificfeedback, $when);
        $options->generalfeedback = self::extract($practise->reviewgeneralfeedback, $when);
        $options->rightanswer = self::extract($practise->reviewrightanswer, $when);
        $options->overallfeedback = self::extract($practise->reviewoverallfeedback, $when);

        $options->numpartscorrect = $options->feedback;
        $options->manualcomment = $options->feedback;

        if ($practise->questiondecimalpoints != -1) {
            $options->markdp = $practise->questiondecimalpoints;
        } else {
            $options->markdp = $practise->decimalpoints;
        }

        return $options;
    }

    protected static function extract($bitmask, $bit,
            $whenset = self::VISIBLE, $whennotset = self::HIDDEN) {
        if ($bitmask & $bit) {
            return $whenset;
        } else {
            return $whennotset;
        }
    }
}


/**
 * A {@link qubaid_condition} for finding all the question usages belonging to
 * a particular practise.
 *
 * @copyright  2010 The Open University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class qubaids_for_practise extends qubaid_join {
    public function __construct($practiseid, $includepreviews = true, $onlyfinished = false) {
        $where = 'practisea.practise = :practiseapractise';
        $params = array('practiseapractise' => $practiseid);

        if (!$includepreviews) {
            $where .= ' AND preview = 0';
        }

        if ($onlyfinished) {
            $where .= ' AND state == :statefinished';
            $params['statefinished'] = practise_attempt::FINISHED;
        }

        parent::__construct('{practise_attempts} practisea', 'practisea.uniqueid', $where, $params);
    }
}

/**
 * Creates a textual representation of a question for display.
 *
 * @param object $question A question object from the database questions table
 * @param bool $showicon If true, show the question's icon with the question. False by default.
 * @param bool $showquestiontext If true (default), show question text after question name.
 *       If false, show only question name.
 * @return string
 */
function practise_question_tostring($question, $showicon = false, $showquestiontext = true) {
    $result = '';

    $name = shorten_text(format_string($question->name), 200);
    if ($showicon) {
        $name .= print_question_icon($question) . ' ' . $name;
    }
    $result .= html_writer::span($name, 'questionname');

    if ($showquestiontext) {
        $questiontext = question_utils::to_plain_text($question->questiontext,
                $question->questiontextformat, array('noclean' => true, 'para' => false));
        $questiontext = shorten_text($questiontext, 200);
        if ($questiontext) {
            $result .= ' ' . html_writer::span(s($questiontext), 'questiontext');
        }
    }

    return $result;
}

/**
 * Verify that the question exists, and the user has permission to use it.
 * Does not return. Throws an exception if the question cannot be used.
 * @param int $questionid The id of the question.
 */
function practise_require_question_use($questionid) {
    global $DB;
    $question = $DB->get_record('question', array('id' => $questionid), '*', MUST_EXIST);
    question_require_capability_on($question, 'use');
}

/**
 * Verify that the question exists, and the user has permission to use it.
 * @param object $practise the practise settings.
 * @param int $slot which question in the practise to test.
 * @return bool whether the user can use this question.
 */
function practise_has_question_use($practise, $slot) {
    global $DB;
    $question = $DB->get_record_sql("
            SELECT q.*
              FROM {practise_slots} slot
              JOIN {question} q ON q.id = slot.questionid
             WHERE slot.practiseid = ? AND slot.slot = ?", array($practise->id, $slot));
    if (!$question) {
        return false;
    }
    return question_has_capability_on($question, 'use');
}

/**
 * Add a question to a practise
 *
 * Adds a question to a practise by updating $practise as well as the
 * practise and practise_slots tables. It also adds a page break if required.
 * @param int $questionid The id of the question to be added
 * @param object $practise The extended practise object as used by edit.php
 *      This is updated by this function
 * @param int $page Which page in practise to add the question on. If 0 (default),
 *      add at the end
 * @param float $maxmark The maximum mark to set for this question. (Optional,
 *      defaults to question.defaultmark.
 * @return bool false if the question was already in the practise
 */
function practise_add_practise_question($questionid, $practise, $page = 0, $maxmark = null) {
    global $DB;
    $slots = $DB->get_records('practise_slots', array('practiseid' => $practise->id),
            'slot', 'questionid, slot, page, id');
    if (array_key_exists($questionid, $slots)) {
        return false;
    }

    $trans = $DB->start_delegated_transaction();

    $maxpage = 1;
    $numonlastpage = 0;
    foreach ($slots as $slot) {
        if ($slot->page > $maxpage) {
            $maxpage = $slot->page;
            $numonlastpage = 1;
        } else {
            $numonlastpage += 1;
        }
    }

    // Add the new question instance.
    $slot = new stdClass();
    $slot->practiseid = $practise->id;
    $slot->questionid = $questionid;

    if ($maxmark !== null) {
        $slot->maxmark = $maxmark;
    } else {
        $slot->maxmark = $DB->get_field('question', 'defaultmark', array('id' => $questionid));
    }

    if (is_int($page) && $page >= 1) {
        // Adding on a given page.
        $lastslotbefore = 0;
        foreach (array_reverse($slots) as $otherslot) {
            if ($otherslot->page > $page) {
                $DB->set_field('practise_slots', 'slot', $otherslot->slot + 1, array('id' => $otherslot->id));
            } else {
                $lastslotbefore = $otherslot->slot;
                break;
            }
        }
        $slot->slot = $lastslotbefore + 1;
        $slot->page = min($page, $maxpage + 1);

        $DB->execute("
                UPDATE {practise_sections}
                   SET firstslot = firstslot + 1
                 WHERE practiseid = ?
                   AND firstslot > ?
                ", array($practise->id, max($lastslotbefore, 1)));

    } else {
        $lastslot = end($slots);
        if ($lastslot) {
            $slot->slot = $lastslot->slot + 1;
        } else {
            $slot->slot = 1;
        }
        if ($practise->questionsperpage && $numonlastpage >= $practise->questionsperpage) {
            $slot->page = $maxpage + 1;
        } else {
            $slot->page = $maxpage;
        }
    }

    $DB->insert_record('practise_slots', $slot);
    $trans->allow_commit();
}

/**
 * Add a random question to the practise at a given point.
 * @param object $practise the practise settings.
 * @param int $addonpage the page on which to add the question.
 * @param int $categoryid the question category to add the question from.
 * @param int $number the number of random questions to add.
 * @param bool $includesubcategories whether to include questoins from subcategories.
 */
function practise_add_random_questions($practise, $addonpage, $categoryid, $number,
        $includesubcategories) {
    global $DB;

    $category = $DB->get_record('question_categories', array('id' => $categoryid));
    if (!$category) {
        print_error('invalidcategoryid', 'error');
    }

    $catcontext = context::instance_by_id($category->contextid);
    require_capability('moodle/question:useall', $catcontext);

    // Find existing random questions in this category that are
    // not used by any practise.
    if ($existingquestions = $DB->get_records_sql(
            "SELECT q.id, q.qtype FROM {question} q
            WHERE qtype = 'random'
                AND category = ?
                AND " . $DB->sql_compare_text('questiontext') . " = ?
                AND NOT EXISTS (
                        SELECT *
                          FROM {practise_slots}
                         WHERE questionid = q.id)
            ORDER BY id", array($category->id, ($includesubcategories ? '1' : '0')))) {
            // Take as many of these as needed.
        while (($existingquestion = array_shift($existingquestions)) && $number > 0) {
            practise_add_practise_question($existingquestion->id, $practise, $addonpage);
            $number -= 1;
        }
    }

    if ($number <= 0) {
        return;
    }

    // More random questions are needed, create them.
    for ($i = 0; $i < $number; $i += 1) {
        $form = new stdClass();
        $form->questiontext = array('text' => ($includesubcategories ? '1' : '0'), 'format' => 0);
        $form->category = $category->id . ',' . $category->contextid;
        $form->defaultmark = 1;
        $form->hidden = 1;
        $form->stamp = make_unique_id_code(); // Set the unique code (not to be changed).
        $question = new stdClass();
        $question->qtype = 'random';
        $question = question_bank::get_qtype('random')->save_question($question, $form);
        if (!isset($question->id)) {
            print_error('cannotinsertrandomquestion', 'practise');
        }
        practise_add_practise_question($question->id, $practise, $addonpage);
    }
}

/**
 * Mark the activity completed (if required) and trigger the course_module_viewed event.
 *
 * @param  stdClass $practise       practise object
 * @param  stdClass $course     course object
 * @param  stdClass $cm         course module object
 * @param  stdClass $context    context object
 * @since Moodle 3.1
 */
function practise_view($practise, $course, $cm, $context) {

    $params = array(
        'objectid' => $practise->id,
        'context' => $context
    );

    $event = \mod_practise\event\course_module_viewed::create($params);
    $event->add_record_snapshot('practise', $practise);
    $event->trigger();

    // Completion.
    $completion = new completion_info($course);
    $completion->set_module_viewed($cm);
}

/**
 * Validate permissions for creating a new attempt and start a new preview attempt if required.
 *
 * @param  practise $practiseobj practise object
 * @param  practise_access_manager $accessmanager practise access manager
 * @param  bool $forcenew whether was required to start a new preview attempt
 * @param  int $page page to jump to in the attempt
 * @param  bool $redirect whether to redirect or throw exceptions (for web or ws usage)
 * @return array an array containing the attempt information, access error messages and the page to jump to in the attempt
 * @throws moodle_practise_exception
 * @since Moodle 3.1
 */
function practise_validate_new_attempt(practise $practiseobj, practise_access_manager $accessmanager, $forcenew, $page, $redirect) {
    global $DB, $USER;
    $timenow = time();

    if ($practiseobj->is_preview_user() && $forcenew) {
        $accessmanager->current_attempt_finished();
    }

    // Check capabilities.
    if (!$practiseobj->is_preview_user()) {
        $practiseobj->require_capability('mod/practise:attempt');
    }

    // Check to see if a new preview was requested.
    if ($practiseobj->is_preview_user() && $forcenew) {
        // To force the creation of a new preview, we mark the current attempt (if any)
        // as finished. It will then automatically be deleted below.
        $DB->set_field('practise_attempts', 'state', practise_attempt::FINISHED,
                array('practise' => $practiseobj->get_practiseid(), 'userid' => $USER->id));
    }

    // Look for an existing attempt.
    $attempts = practise_get_user_attempts($practiseobj->get_practiseid(), $USER->id, 'all', true);
    $lastattempt = end($attempts);

    $attemptnumber = null;
    // If an in-progress attempt exists, check password then redirect to it.
    if ($lastattempt && ($lastattempt->state == practise_attempt::IN_PROGRESS ||
            $lastattempt->state == practise_attempt::OVERDUE)) {
        $currentattemptid = $lastattempt->id;
        $messages = $accessmanager->prevent_access();

        // If the attempt is now overdue, deal with that.
        $practiseobj->create_attempt_object($lastattempt)->handle_if_time_expired($timenow, true);

        // And, if the attempt is now no longer in progress, redirect to the appropriate place.
        if ($lastattempt->state == practise_attempt::ABANDONED || $lastattempt->state == practise_attempt::FINISHED) {
            if ($redirect) {
                redirect($practiseobj->review_url($lastattempt->id));
            } else {
                throw new moodle_practise_exception($practiseobj, 'attemptalreadyclosed');
            }
        }

        // If the page number was not explicitly in the URL, go to the current page.
        if ($page == -1) {
            $page = $lastattempt->currentpage;
        }

    } else {
        while ($lastattempt && $lastattempt->preview) {
            $lastattempt = array_pop($attempts);
        }

        // Get number for the next or unfinished attempt.
        if ($lastattempt) {
            $attemptnumber = $lastattempt->attempt + 1;
        } else {
            $lastattempt = false;
            $attemptnumber = 1;
        }
        $currentattemptid = null;

        $messages = $accessmanager->prevent_access() +
            $accessmanager->prevent_new_attempt(count($attempts), $lastattempt);

        if ($page == -1) {
            $page = 0;
        }
    }
    return array($currentattemptid, $attemptnumber, $lastattempt, $messages, $page);
}

/**
 * Prepare and start a new attempt deleting the previous preview attempts.
 *
 * @param  practise $practiseobj practise object
 * @param  int $attemptnumber the attempt number
 * @param  object $lastattempt last attempt object
 * @return object the new attempt
 * @since  Moodle 3.1
 */
function practise_prepare_and_start_new_attempt(practise $practiseobj, $attemptnumber, $lastattempt) {
    global $DB, $USER;

    // Delete any previous preview attempts belonging to this user.
    practise_delete_previews($practiseobj->get_practise(), $USER->id);

    $quba = question_engine::make_questions_usage_by_activity('mod_practise', $practiseobj->get_context());
    $quba->set_preferred_behaviour($practiseobj->get_practise()->preferredbehaviour);

    // Create the new attempt and initialize the question sessions
    $timenow = time(); // Update time now, in case the server is running really slowly.
    $attempt = practise_create_attempt($practiseobj, $attemptnumber, $lastattempt, $timenow, $practiseobj->is_preview_user());

    if (!($practiseobj->get_practise()->attemptonlast && $lastattempt)) {
        $attempt = practise_start_new_attempt($practiseobj, $quba, $attempt, $attemptnumber, $timenow);
    } else {
        $attempt = practise_start_attempt_built_on_last($quba, $attempt, $lastattempt);
    }

    $transaction = $DB->start_delegated_transaction();

    $attempt = practise_attempt_save_started($practiseobj, $quba, $attempt);

    $transaction->allow_commit();

    return $attempt;
}
