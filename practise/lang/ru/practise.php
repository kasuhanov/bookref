<?php


$string['modulename'] = 'Упражнения';
$string['modulenameplural'] = 'Упражнения';
$string['modulename_help'] = 'Use the practise module for... | The practise module allows...';
$string['practise:addinstance'] = 'Добавить новую Упражнения';
$string['practise:submit'] = 'Сохранить Упражнения';
$string['practise:view'] = 'View practise';
$string['practisefieldset'] = 'Custom example fieldset';
$string['practise_name'] = 'Заголовок';
$string['practise_content'] = 'Содержимое';
$string['practise'] = 'practise';
$string['nopractises'] = 'nopractises';
$string['pluginadministration'] = 'practise administration';
$string['pluginname'] = 'Упражнения';
$string['type'] = 'Тип';
$string['noquestions'] = 'No questions have been added yet';
$string['editpractise'] = 'Edit practise';

