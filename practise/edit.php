<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Page to edit practisezes
 *
 * This page generally has two columns:
 * The right column lists all available questions in a chosen category and
 * allows them to be edited or more to be added. This column is only there if
 * the practise does not already have student attempts
 * The left column lists all questions that have been added to the current practise.
 * The lecturer can add questions from the right hand list to the practise or remove them
 *
 * The script also processes a number of actions:
 * Actions affecting a practise:
 * up and down  Changes the order of questions and page breaks
 * addquestion  Adds a single question to the practise
 * add          Adds several selected questions to the practise
 * addrandom    Adds a certain number of random questions to the practise
 * repaginate   Re-paginates the practise
 * delete       Removes a question from the practise
 * savechanges  Saves the order and grades for questions in the practise
 *
 * @package    mod_practise
 * @copyright  1999 onwards Martin Dougiamas and others {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once(__DIR__ . '/../../config.php');
//require_once($CFG->dirroot . '/mod/practise/locallib.php');
//require_once($CFG->dirroot . '/mod/practise/addrandomform.php');
require_once($CFG->dirroot . '/question/editlib.php');
require_once($CFG->dirroot . '/question/category_class.php');
require_once(dirname(__FILE__).'/edit_form.php');

// These params are only passed from page request to request while we stay on
// this page otherwise they would go in question_edit_setup.
$scrollpos = optional_param('scrollpos', '', PARAM_INT);

list($thispageurl, $contexts, $cmid, $cm, $practise, $pagevars) =
        question_edit_setup('editq', '/mod/practise/edit.php', true);

$defaultcategoryobj = question_make_default_categories($contexts->all());
$defaultcategory = $defaultcategoryobj->id . ',' . $defaultcategoryobj->contextid;

//$practisehasattempts = practise_has_attempts($practise->id);
$practisehasattempts = true;

$PAGE->set_url($thispageurl);

// Get the course object and related bits.
$course = $DB->get_record('course', array('id' => $practise->course), '*', MUST_EXIST);
//$practiseobj = new practise($practise, $cm, $course);
//$structure = $practiseobj->get_structure();

// You need mod/practise:manage in addition to question capabilities to access this page.
//require_capability('mod/practise:manage', $contexts->lowest());

// Process commands ============================================================.

// Get the list of question ids had their check-boxes ticked.
$selectedslots = array();
$params = (array) data_submitted();
foreach ($params as $key => $value) {
    if (preg_match('!^s([0-9]+)$!', $key, $matches)) {
        $selectedslots[] = $matches[1];
    }
}

$afteractionurl = new moodle_url($thispageurl);
if ($scrollpos) {
    $afteractionurl->param('scrollpos', $scrollpos);
}

if (optional_param('repaginate', false, PARAM_BOOL) && confirm_sesskey()) {
    // Re-paginate the practise.
    $structure->check_can_be_edited();
    $questionsperpage = optional_param('questionsperpage', $practise->questionsperpage, PARAM_INT);
    practise_repaginate_questions($practise->id, $questionsperpage );
    practise_delete_previews($practise);
    redirect($afteractionurl);
}

if (($addquestion = optional_param('addquestion', 0, PARAM_INT)) && confirm_sesskey()) {
    // Add a single question to the current practise.
    $structure->check_can_be_edited();
    practise_require_question_use($addquestion);
    $addonpage = optional_param('addonpage', 0, PARAM_INT);
    practise_add_practise_question($addquestion, $practise, $addonpage);
    practise_delete_previews($practise);
    practise_update_sumgrades($practise);
    $thispageurl->param('lastchanged', $addquestion);
    redirect($afteractionurl);
}

if (optional_param('add', false, PARAM_BOOL) && confirm_sesskey()) {
    $structure->check_can_be_edited();
    $addonpage = optional_param('addonpage', 0, PARAM_INT);
    // Add selected questions to the current practise.
    $rawdata = (array) data_submitted();
    foreach ($rawdata as $key => $value) { // Parse input for question ids.
        if (preg_match('!^q([0-9]+)$!', $key, $matches)) {
            $key = $matches[1];
            practise_require_question_use($key);
            practise_add_practise_question($key, $practise, $addonpage);
        }
    }
    practise_delete_previews($practise);
    practise_update_sumgrades($practise);
    redirect($afteractionurl);
}

if ($addsectionatpage = optional_param('addsectionatpage', false, PARAM_INT)) {
    // Add a section to the practise.
    $structure->check_can_be_edited();
    $structure->add_section_heading($addsectionatpage);
    practise_delete_previews($practise);
    redirect($afteractionurl);
}

if ((optional_param('addrandom', false, PARAM_BOOL)) && confirm_sesskey()) {
    // Add random questions to the practise.
    $structure->check_can_be_edited();
    $recurse = optional_param('recurse', 0, PARAM_BOOL);
    $addonpage = optional_param('addonpage', 0, PARAM_INT);
    $categoryid = required_param('categoryid', PARAM_INT);
    $randomcount = required_param('randomcount', PARAM_INT);
    practise_add_random_questions($practise, $addonpage, $categoryid, $randomcount, $recurse);

    practise_delete_previews($practise);
    practise_update_sumgrades($practise);
    redirect($afteractionurl);
}

if (optional_param('savechanges', false, PARAM_BOOL) && confirm_sesskey()) {

    // If rescaling is required save the new maximum.
    $maxgrade = unformat_float(optional_param('maxgrade', -1, PARAM_RAW));
    if ($maxgrade >= 0) {
        practise_set_grade($maxgrade, $practise);
        practise_update_all_final_grades($practise);
        practise_update_grades($practise, 0, true);
    }

    redirect($afteractionurl);
}

// Get the question bank view.
//$questionbank = new mod_practise\question\bank\custom_view($contexts, $thispageurl, $course, $cm, $practise);
//$questionbank->set_practise_has_attempts($practisehasattempts);
//$questionbank->process_actions($thispageurl, $cm);

// End of process commands =====================================================.

$PAGE->set_pagelayout('incourse');
$PAGE->set_pagetype('mod-practise-edit');

$output = $PAGE->get_renderer('mod_practise');

$PAGE->set_title(get_string('editingpractisex', 'practise', format_string($practise->name)));
$PAGE->set_heading($course->fullname);
$node = $PAGE->settingsnav->find('mod_practise_edit', navigation_node::TYPE_SETTING);
if ($node) {
    $node->make_active();
}
echo $OUTPUT->header();

// Initialise the JavaScript.
$practiseeditconfig = new stdClass();
$practiseeditconfig->url = $thispageurl->out(true, array('qbanktool' => '0'));
$practiseeditconfig->dialoglisteners = array();
//$numberoflisteners = $DB->get_field_sql("
//    SELECT COALESCE(MAX(page), 1)
//      FROM {practise_slots}
//     WHERE practiseid = ?", array($practise->id));

//for ($pageiter = 1; $pageiter <= $numberoflisteners; $pageiter++) {
//    $practiseeditconfig->dialoglisteners[] = 'addrandomdialoglaunch_' . $pageiter;
//}

$PAGE->requires->data_for_js('practise_edit_config', $practiseeditconfig);
$PAGE->requires->js('/question/qengine.js');

// Questions wrapper start.
echo html_writer::start_tag('div', array('class' => 'mod-practise-edit-content'));

//echo $output->edit_page($practiseobj, $structure, $contexts, $thispageurl, $pagevars);
$qwe = $DB->get_field_sql("
    SELECT COALESCE(MAX(id), 1)
      FROM {course}
     ");

//$randomform = new \quiz_add_random_form(new \moodle_url('/mod/quiz/addrandom.php'),
//    array('contexts' => $contexts, 'cat' => $pagevars['cat']));
//$randomform->set_data(array(
//    'category' => $pagevars['cat'],
//    'returnurl' => $thispageurl->out_as_local_url(true),
//    'randomnumber' => 1,
//    'cmid' => $thispageurl->param('cmid'),
//));
//return html_writer::div($randomform->render(), 'randomquestionformforpopup');
//
//if ($mform->is_cancelled()) {
//} else if ($fromform = $mform->get_data()) {
//} else {
//    $mform->set_data($toform);
//    $mform->display();
//}
//$mform->display();
echo $qwe;
//echo $output->edit_page();
$renderable = new \practise\index_page('Some text');
echo $output->render($renderable);
// Questions wrapper end.
echo html_writer::end_tag('div');

echo $OUTPUT->footer();


