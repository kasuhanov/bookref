<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of practise
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_practise
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Replace practise with the name of your module and remove this line.

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');
require_once(dirname(__FILE__).'/renderer.php');


$id = optional_param('id', 0, PARAM_INT); // Course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // ... practise instance ID - it should be named as the first character of the module.

if ($id) {
    $cm         = get_coursemodule_from_id('practise', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $practise  = $DB->get_record('practise', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $practise  = $DB->get_record('practise', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $practise->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('practise', $practise->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);

$PAGE->set_url('/mod/practise/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($practise->title));
$PAGE->set_heading(format_string($course->fullname));

$viewobj = new mod_practise_view_object();
$context = context_module::instance($cm->id);

//$viewobj->canedit = has_capability('mod/practise:manage', $context);
$viewobj->canedit = true;
$viewobj->editurl = new moodle_url('/mod/practise/edit.php', array('cmid' => $cm->id));
$viewobj->backtocourseurl = new moodle_url('/course/view.php', array('id' => $course->id));
$viewobj->popuprequired = false;
$viewobj->popupoptions = false;
$viewobj->practisehasquestions =  false;

$output = $PAGE->get_renderer('mod_practise');

//$canedit = has_capability('mod/practise:manage', $context);
//$output .= $this->notification(get_string('noquestions', 'quiz'));
//if ($canedit) {
//    $output .= $this->single_button($editurl, get_string('editquiz', 'quiz'), 'get');
//}
$viewobj->infomessages = "";
$viewobj->showbacktocourse = true;

echo $OUTPUT->header();
echo $OUTPUT->heading($practise->name);
//echo $output;
echo $output->view_page($course, $quiz, $cm, $context, $viewobj);

echo $OUTPUT->footer();
