<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_practise
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Returns the information on whether the module supports a feature
 *
 * See {@link plugin_supports()} for more info.
 *
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function practise_supports($feature) {

    switch($feature) {
//        case FEATURE_MOD_ARCHETYPE:     return MOD_ARCHETYPE_RESOURCE;
//        case FEATURE_MOD_INTRO:         return true;
//      case FEATURE_SHOW_DESCRIPTION:  return true;
        default:                        return null;
    }
}

/**
 * Saves a new instance of the practise into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param stdClass $practise Submitted data from the form in mod_form.php
 * @param mod_practise_mod_form $mform The form instance itself (if needed)
 * @return int The id of the newly inserted practise record
 */
function practise_add_instance(stdClass $practise, mod_practise_mod_form $mform = null) {
    global $DB;

    $practise->timecreated = time();
    $practise->timemodified = $practise->timecreated;

    return $DB->insert_record('practise', $practise);
}

/**
 * Updates an instance of the practise in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param stdClass $practise An object from the form in mod_form.php
 * @param mod_practise_mod_form $mform The form instance itself (if needed)
 * @return boolean Success/Fail
 */
function practise_update_instance(stdClass $practise, mod_practise_mod_form $mform = null){
    global $DB;

    $practise->timemodified = time();
    $practise->id = $practise->instance;

    return $DB->update_record('practise', $practise);
}

/**
 * This standard function will check all instances of this module
 * and make sure there are up-to-date events created for each of them.
 * If courseid = 0, then every practise event in the site is checked, else
 * only practise events belonging to the course specified are checked.
 * This is only required if the module is generating calendar events.
 *
 * @param int $courseid Course ID
 * @return bool
 */
function practise_refresh_events($courseid = 0) {
    global $DB;

    if ($courseid == 0) {
        if (!$practises = $DB->get_records('practise')) {
            return true;
        }
    } else {
        if (!$practises = $DB->get_records('practise', array('course' => $courseid))) {
            return true;
        }
    }

    foreach ($practises as $practise) {
        // Create a function such as the one below to deal with updating calendar events.
        // practise_update_events($practise);
    }

    return true;
}

/**
 * Removes an instance of the practise from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function practise_delete_instance($id) {
    global $DB;

    if (! $practise = $DB->get_record('practise', array('id' => $id))) {
        return false;
    }

    // Delete any dependent records here.

    $DB->delete_records('practise', array('id' => $practise->id));

    return true;
}

/**
 * Returns a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 *
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @param stdClass $course The course record
 * @param stdClass $user The user record
 * @param cm_info|stdClass $mod The course module info object or record
 * @param stdClass $practise The practise instance record
 * @return stdClass|null
 */
function practise_user_outline($course, $user, $mod, $practise) {

    $return = new stdClass();
    $return->time = 0;
    $return->info = '';
    return $return;
}

/**
 * Prints a detailed representation of what a user has done with
 * a given particular instance of this module, for user activity reports.
 *
 * It is supposed to echo directly without returning a value.
 *
 * @param stdClass $course the current course record
 * @param stdClass $user the record of the user we are generating report for
 * @param cm_info $mod course module info
 * @param stdClass $practise the module instance record
 */
function practise_user_complete($course, $user, $mod, $practise) {
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in practise activities and print it out.
 *
 * @param stdClass $course The course record
 * @param bool $viewfullnames Should we display full names
 * @param int $timestart Print activity since this timestamp
 * @return boolean True if anything was printed, otherwise false
 */
function practise_print_recent_activity($course, $viewfullnames, $timestart) {
    return false;
}

/**
 * Prepares the recent activity data
 *https://webauth.susu.ac.ru/login.html?redirect=www.gstatic.com/generate_204
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link practise_print_recent_mod_activity()}.
 *
 * Returns void, it adds items into $activities and increases $index.
 *
 * @param array $activities sequentially indexed array of objects with added 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $courseid the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 */
function practise_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {@link bookrehttps://webauth.susu.ac.ru/login.html?redirect=www.gstatic.com/generate_204f_get_recent_mod_activity()}
 *
 * @param stdClass $activity activity record with added 'cmid' property
 * @param int $courseid the id of the course we produce the report for
 * @param bool $detail print detailed report
 * @param array $modnames as returned by {@link get_module_types_names()}
 * @param bool $viewfullnames display users' full names
 */
function practise_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
}

/**
 * Function to be run periodically according to the moodle cron
 *
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc ...
 *
 * Note that this has been deprecated in favour of scheduled task API.
 *
 * @return boolean
 */
function practise_cron () {
    return true;
}

/**
 * Returns all other caps used in the module
 *
 * For example, this could be array('moodle/site:accessallgroups') if the
 * module uses that capability.
 *
 * @return array
 */
function practise_get_extra_capabilities() {
    return array();
}
