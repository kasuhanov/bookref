<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * bookref module renderer
 *
 * @package   mod_bookref
 * @copyright 2009 Petr Skoda  {@link http://skodak.org}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

class mod_bookref_renderer extends plugin_renderer_base {

    /**
     * Returns html to display the content of mod_bookref
     * (Description, bookref files and optionally Edit button)
     *
     * @param stdClass $bookref record from 'bookref' table (please note
     *     it may not contain fields 'revision' and 'timemodified')
     * @return string
     */
    public function display_bookref(stdClass $bookref) {
        $output = '';
        $bookrefinstances = get_fast_modinfo($bookref->course)->get_instances_of('bookref');
        if (!isset($bookrefinstances[$bookref->id]) ||
                !($cm = $bookrefinstances[$bookref->id]) ||
                !($context = context_module::instance($cm->id))) {
            // Some error in parameters.
            // Don't throw any errors in renderer, just return empty string.
            // Capability to view module must be checked before calling renderer.
            return $output;
        }

//        if (trim($bookref->intro)) {
//            if ($bookref->display != bookref_DISPLAY_INLINE) {
//                $output .= $this->output->box(format_module_intro('bookref', $bookref, $cm->id),
//                        'generalbox', 'intro');
//            } else if ($cm->showdescription) {
//                // for "display inline" do not filter, filters run at display time.
//                $output .= format_module_intro('bookref', $bookref, $cm->id, false);
//            }
//        }

        $bookreftree = new bookref_tree($bookref, $cm);
//        if ($bookref->display == bookref_DISPLAY_INLINE) {
            // Display module name as the name of the root directory.
            $bookreftree->dir['dirname'] = $cm->get_formatted_name();
//        }
        $output .= $this->output->box($this->render($bookreftree),
                'generalbox bookreftree');

        // Do not append the edit button on the course page.
//        if ($bookref->display != bookref_DISPLAY_INLINE) {
//            $containercontents = '';
//            $downloadable = bookref_archive_available($bookref, $cm);
//
//            if ($downloadable) {
//                $downloadbutton = $this->output->single_button(
//                    new moodle_url('/mod/bookref/download_bookref.php', array('id' => $cm->id)),
//                    get_string('downloadbookref', 'bookref')
//                );
//
//                $output .= $this->output->container(
//                    $downloadbutton,
//                    'mdl-align bookref-download-button');
//            }
//
//            if (has_capability('mod/bookref:managefiles', $context)) {
//                $editbutton = $this->output->single_button(
//                    new moodle_url('/mod/bookref/edit.php', array('id' => $cm->id)),
//                    get_string('edit')
//                );
//
//                $output .= $this->output->container(
//                    $editbutton,
//                    'mdl-align bookref-edit-button');
//            }
//        }
        return $output;
    }

    public function render_bookref_tree(bookref_tree $tree) {
        static $treecounter = 0;

        $content = '';
        $id = 'bookref_tree'. ($treecounter++);
        $content .= '<div id="'.$id.'" class="filemanager">';
        $content .= $this->htmllize_tree($tree, array('files' => array(), 'subdirs' => array($tree->dir)));
        $content .= '</div>';
        $showexpanded = true;
        if (empty($tree->bookref->showexpanded)) {
            $showexpanded = false;
        }
//        $this->page->requires->js_init_call('M.mod_bookref.init_tree', array($id, $showexpanded));
        return $content;
    }

    /**
     * Internal function - creates htmls structure suitable for YUI tree.
     */
    protected function htmllize_tree($tree, $dir) {
        global $CFG;

        if (empty($dir['subdirs']) and empty($dir['files'])) {
            return '';
        }
        $result = '<ul>';
        foreach ($dir['subdirs'] as $subdir) {
            $image = $this->output->pix_icon(file_folder_icon(24), $subdir['dirname'], 'moodle');
            $filename = html_writer::tag('span', $image, array('class' => 'fp-icon')).
                    html_writer::tag('span', s($subdir['dirname']), array('class' => 'fp-filename'));
            $filename = html_writer::tag('div', $filename, array('class' => 'fp-filename-icon'));
            $result .= html_writer::tag('li', $filename. $this->htmllize_tree($tree, $subdir));
        }
        foreach ($dir['files'] as $file) {
            $filename = $file->get_filename();
            $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(),
                    $file->get_filearea(), $file->get_itemid(), $file->get_filepath(), $filename, false);
            if (file_extension_in_typegroup($filename, 'web_image')) {
                $image = $url->out(false, array('preview' => 'tinyicon', 'oid' => $file->get_timemodified()));
                $image = html_writer::empty_tag('img', array('src' => $image));
            } else {
                $image = $this->output->pix_icon(file_file_icon($file, 24), $filename, 'moodle');
            }
            $filename = html_writer::tag('span', $image, array('class' => 'fp-icon')).
                    html_writer::tag('span', $filename, array('class' => 'fp-filename'));
            $filename = html_writer::tag('span',
                    html_writer::link($url->out(false, array('forcedownload' => 1)), $filename),
                    array('class' => 'fp-filename-icon'));
            $result .= html_writer::tag('li', $filename);
        }
        $result .= '</ul>';

        return $result;
    }
}

class bookref_tree implements renderable {
    public $context;
    public $bookref;
    public $cm;
    public $dir;

    public function __construct($bookref, $cm) {
        $this->bookref = $bookref;
        $this->cm     = $cm;

        $this->context = context_module::instance($cm->id);
        $fs = get_file_storage();
        $this->dir = $fs->get_area_tree($this->context->id, 'mod_bookref', 'content', 0);
    }
}
