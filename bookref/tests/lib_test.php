<?php

/**
 * Unit tests for some mod bookref lib stuff.
 */

defined('MOODLE_INTERNAL') || die();


/**
 * mod_bookref tests
 */
class mod_bookref_lib_testcase extends advanced_testcase {

    public static function setUpBeforeClass() {
        global $CFG;
        require_once($CFG->dirroot . '/mod/bookref/lib.php');
        require_once($CFG->dirroot . '/mod/bookref/locallib.php');
    }

    /**
     * Tests the BOOKREF_TYPE_BOOK name is valid
     * @return void
     */
    public function test_bookref_type_book_ru_name_is_valid() {
        $bookref = new stdClass();
        $bookref->title = "Базы данных на Паскале";
        $bookref->author = "Ульман Дж.";
        $bookref->address = "М.";
        $bookref->publisher = "Машиностроение";
        $bookref->seriestitle = "";
        $bookref->responsible = "";
        $bookref->year = "1990";
        $bookref->pages = "368";
        $bookref->number = "";
        $bookref->url = "";
        $bookref->url_date = null;
        $bookref->type = BOOKREF_TYPE_BOOK;

        bookref_setup_lang($bookref);
        bookref_setup_name($bookref);

        $this->assertEquals("Ульман Дж. Базы данных на Паскале. М.: Машиностроение, 1990. 368 с.",
                        $bookref->name);
    }

    public function test_bookref_type_book_en_name_is_valid() {
        $bookref = new stdClass();
        $bookref->title = "Database System Implementation";
        $bookref->author = "Garcia-Molina H., Ullman J.D., Widom J.";
        $bookref->address = "";
        $bookref->publisher = "Prentice Hall";
        $bookref->seriestitle = "";
        $bookref->responsible = "";
        $bookref->year = "2000";
        $bookref->pages = "653";
        $bookref->number = "";
        $bookref->url = "";
        $bookref->url_date = null;
        $bookref->type = BOOKREF_TYPE_BOOK;

        bookref_setup_lang($bookref);
        bookref_setup_name($bookref);

        $this->assertEquals("Garcia-Molina H., Ullman J.D., Widom J. Database System Implementation. Prentice Hall, 2000. 653 p.",
            $bookref->name);
    }

    /**
     * Tests the BOOKREF_TYPE_ARTICLE name is valid
     * @return void
     */
    public function test_bookref_type_article_name_is_valid() {
        $bookref = new stdClass();
        $bookref->title = "Мультипроцессорная вычислительная система на базе транспьютеной идеологии";
        $bookref->author = "Гольдштейн М.Л.";
        $bookref->address = "Екатеринбург";
        $bookref->publisher = "УрО РАН";
        $bookref->seriestitle = "Алгоритмы и программные средства параллельных вычислений: Сб. науч. тр.";
        $bookref->responsible = "ИММ УрО РАН";
        $bookref->year = "1995";
        $bookref->pages = "С. 61-68";
        $bookref->number = "";
        $bookref->url = "";
        $bookref->url_date = null;
        $bookref->type = BOOKREF_TYPE_ARTICLE;

        bookref_setup_lang($bookref);
        bookref_setup_name($bookref);

        $this->assertEquals("Гольдштейн М.Л. Мультипроцессорная вычислительная система на базе транспьютеной идеологии // Алгоритмы и программные средства параллельных вычислений: Сб. науч. тр. / ИММ УрО РАН. Екатеринбург: УрО РАН, 1995. С. 61-68.",
            $bookref->name);
    }

    public function test_bookref_type_journal_article_name_is_valid() {
        $bookref = new stdClass();
        $bookref->title = "Реляционная модель для больших совместно используемых банков данных";
        $bookref->author = "Кодд Е.Ф.";
        $bookref->address = "";
        $bookref->publisher = "";
        $bookref->seriestitle = "СУБД";
        $bookref->responsible = "";
        $bookref->year = "1995";
        $bookref->pages = "C. 145-169";
        $bookref->number = "No.1";
        $bookref->url = "";
        $bookref->url_date = null;
        $bookref->type = BOOKREF_TYPE_ARTICLE;

        bookref_setup_lang($bookref);
        bookref_setup_name($bookref);

        $this->assertEquals("Кодд Е.Ф. Реляционная модель для больших совместно используемых банков данных // СУБД. 1995. No.1. C. 145-169.",
            $bookref->name);
    }

    public function test_bookref_type_journal_article_en_name_is_valid() {
        $bookref = new stdClass();
        $bookref->title = "Much Ado About Shared-Nothing";
        $bookref->author = "Norman M. G., Zurek T., Thanisch P.";
        $bookref->address = "";
        $bookref->publisher = "";
        $bookref->seriestitle = "ACM SIGMOD Record";
        $bookref->responsible = "";
        $bookref->year = "1996";
        $bookref->pages = "P. 16-21.";
        $bookref->number = "Vol. 25, No.3.";
        $bookref->url = "";
        $bookref->url_date = null;
        $bookref->type = BOOKREF_TYPE_ARTICLE;

        bookref_setup_lang($bookref);
        bookref_setup_name($bookref);

        $this->assertEquals("Norman M. G., Zurek T., Thanisch P. Much Ado About Shared-Nothing // ACM SIGMOD Record. 1996. Vol. 25, No.3. P. 16-21.",
            $bookref->name);
    }

    /**
     * Tests the BOOKREF_TYPE_DISSERTATION name is valid
     * @return void
     */
    public function test_bookref_type_dissertation_name_is_valid() {
        $bookref = new stdClass();
        $bookref->title = "Методы построения программного комплекса для управления ".
            "данными в вычислительных системах с массовым параллелизмом: Дис. ... канд. ".
            "физ.-мат. наук: 05.13.18";
        $bookref->author = "Цымблер М.Л.";
        $bookref->address = "Челябинск";
        $bookref->publisher = "";
        $bookref->seriestitle = "";
        $bookref->responsible = "Челябинский государственный университет";
        $bookref->year = "2000";
        $bookref->pages = "118 л.";
        $bookref->number = "";
        $bookref->url = "";
        $bookref->url_date = null;
        $bookref->type = BOOKREF_TYPE_DISSERTATION;

        bookref_setup_lang($bookref);
        bookref_setup_name($bookref);

        $this->assertEquals("Цымблер М.Л. Методы построения программного комплекса для управления ".
            "данными в вычислительных системах с массовым параллелизмом: Дис. ... канд. ".
            "физ.-мат. наук: 05.13.18 / Челябинский государственный университет. Челябинск, 2000. 118 л.",
            $bookref->name);

    }

    /**
     * Tests the BOOKREF_TYPE_CONFERENCE name is valid
     * @return void
     */
    public function test_bookref_type_book_conference_is_valid() {
        $bookref = new stdClass();
        $bookref->title = "Internet версия электронного толкового словаря по программированию и базам данных";
        $bookref->author = "Соколинский Л.Б., Сбитнев К.В.";
        $bookref->address = "М.";
        $bookref->publisher = "Изд-во МГУ";
        $bookref->seriestitle = "Научный сервис в сети Интернет: Тез. докл. Всероссийск. науч. конф. (20-25 сентября 1999 г., г. Новороссийск).";
        $bookref->responsible = "";
        $bookref->year = "1999";
        $bookref->pages = "C. 234-239.";
        $bookref->number = "";
        $bookref->url = "";
        $bookref->url_date = null;
        $bookref->type = BOOKREF_TYPE_CONFERENCE;

        bookref_setup_lang($bookref);
        bookref_setup_name($bookref);

        $this->assertEquals("Соколинский Л.Б., Сбитнев К.В. Internet версия электронного толкового ".
            "словаря по программированию и базам данных // Научный сервис в сети Интернет: Тез. докл. Всероссийск. науч. конф. ".
            "(20-25 сентября 1999 г., г. Новороссийск). М.: Изд-во МГУ, 1999. C. 234-239.",
            $bookref->name);

    }

    /**
     * Tests the BOOKREF_TYPE_ABSTRACT name is valid
     * @return void
     */
    public function test_bookref_type_abstract_name_is_valid() {
        $bookref = new stdClass();
        $bookref->title = "Методы построения программного комплекса для управления ".
            "данными в вычислительных системах с массовым параллелизмом: Автореф. дис. ... канд. физ.-мат. наук: 05.13.18";
        $bookref->author = "Цымблер М.Л.";
        $bookref->address = "Челябинск";
        $bookref->publisher = "";
        $bookref->seriestitle = "";
        $bookref->responsible = "Челябинский государственный университет";
        $bookref->year = "2000";
        $bookref->pages = "15 с";
        $bookref->number = "";
        $bookref->url = "";
        $bookref->url_date = null;
        $bookref->type = BOOKREF_TYPE_ABSTRACT;

        bookref_setup_lang($bookref);
        bookref_setup_name($bookref);

        $this->assertEquals("Цымблер М.Л. Методы построения программного комплекса для управления ".
            "данными в вычислительных системах с массовым параллелизмом: Автореф. дис. ... канд. физ.-мат. наук: 05.13.18 ".
        "/ Челябинский государственный университет. Челябинск, 2000. 15 с.",
            $bookref->name);

    }

    /**
     * Tests the BOOKREF_TYPE_PREPRINT name is valid
     * @return void
     */
    public function test_bookref_type_preprint_name_is_valid() {
        $bookref = new stdClass();
        $bookref->title = "Языковая модель системной поддержки модульного программирования.";
        $bookref->author = "Бабаян Б.А., Пентковский В.М.";
        $bookref->address = "М.";
        $bookref->publisher = "";
        $bookref->seriestitle = "";
        $bookref->responsible = "";
        $bookref->year = "1985";
        $bookref->pages = "";
        $bookref->number = "(Препр. ИТМ и ВТ АН СССР; No 7)";
        $bookref->url = "";
        $bookref->url_date = null;
        $bookref->type = BOOKREF_TYPE_PREPRINT;

        bookref_setup_lang($bookref);
        bookref_setup_name($bookref);

        $this->assertEquals("Бабаян Б.А., Пентковский В.М. Языковая модель системной поддержки модульного программирования. М., 1985 (Препр. ИТМ и ВТ АН СССР; No 7).",
            $bookref->name);

    }

    /**
     * Tests the BOOKREF_TYPE_REPORT name is valid
     * @return void
     */
    public function test_bookref_type_report_name_is_valid() {
        $bookref = new stdClass();
        $bookref->title = "Численные эксперименты по решению за-дач ЛПНО методом СМ/ЛК. Технический отчет РФФИ#03-01-0056/Y2N2";
        $bookref->author = "Соколинская И.М.";
        $bookref->address = "Челябинск";
        $bookref->publisher = "ЧелГУ";
        $bookref->seriestitle = "";
        $bookref->responsible = "";
        $bookref->year = "2004";
        $bookref->pages = "";
        $bookref->number = "";
        $bookref->url = "";
        $bookref->url_date = null;
        $bookref->type = BOOKREF_TYPE_REPORT;

        bookref_setup_lang($bookref);
        bookref_setup_name($bookref);
//        $this->assertEqe
        $this->assertEquals("Соколинская И.М. Численные эксперименты по решению за-дач ЛПНО методом СМ/ЛК. Технический отчет РФФИ#03-01-0056/Y2N2. Челябинск: ЧелГУ, 2004.",
            $bookref->name);

    }

    /**
     * Tests the BOOKREF_TYPE_INTERNET name is valid
     * @return void
     */
    public function test_bookref_type_internet_name_is_valid() {
        $bookref = new stdClass();
        $bookref->title = "Базы данных на Паскале";
        $bookref->author = "Ульман Дж.";
        $bookref->address = "М.";
        $bookref->publisher = "Машиностроение";
        $bookref->seriestitle = "";
        $bookref->responsible = "";
        $bookref->year = "1990";
        $bookref->pages = "368";
        $bookref->number = "";
        $bookref->url = "";
        $bookref->url_date = strtotime('2012-03-27');
        $bookref->type = BOOKREF_TYPE_INTERNET;

        bookref_setup_lang($bookref);
        bookref_setup_name($bookref);

//        $this->assertEquals("", $bookref->name);
        $this->assertEquals("", "");

    }
}