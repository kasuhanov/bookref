<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');
//for url validation
require_once($CFG->dirroot.'/mod/url/locallib.php');

/**
 * Module instance settings form
 *
 * @package    mod_bookref
 * @copyright  2016 Your title <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_bookref_mod_form extends moodleform_mod {

    public function definition() {
        global $CFG;

        $mform = $this->_form;

        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'title', get_string('bookref_title', 'bookref'), array('size' => '64'));

        $mform->setType('title', PARAM_TEXT);
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('title', PARAM_TEXT);
        } else {
            $mform->setType('title', PARAM_CLEANHTML);
        }
        $mform->addRule('title', null, 'required', null, 'client');
        $mform->addRule('title', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('textarea', 'author', get_string('bookref_author', 'bookref'), 'rows="2" cols="67"');
        $mform->setType('author', PARAM_RAW);
        $mform->addRule('author', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('author', 'bookref_author', 'bookref');

//        $mform->disabledIf('author', 'type', 'eq', BOOKREF_TYPE_BOOK);
        $options = array(1=>'sdfsdf',2=>'adsdas');
//        if ($outcomes = grade_outcome::fetch_all_available($COURSE->id)) {
//            foreach ($outcomes as $outcome) {
//                $options[$outcome->id] = $outcome->get_name();
//            }
//        }
        $select = $mform->addElement('selectwithlink', 'authors', get_string('scale'), $options, null,
            array('link' => $CFG->wwwroot.'/grade/edit/scale/edit.php?courseid='.$COURSE->id, 'label' => get_string('scalescustomcreate')));
        $select->setMultiple(true);

        $mform->addElement('select', 'type', get_string('type', 'mod_bookref'),
                        array(
                            BOOKREF_TYPE_BOOK => get_string('bookreftype_book', 'mod_bookref'),
                            BOOKREF_TYPE_ARTICLE => get_string('bookreftype_article', 'mod_bookref'),
                            BOOKREF_TYPE_DISSERTATION => get_string('bookreftype_dissertation', 'mod_bookref'),
                            BOOKREF_TYPE_CONFERENCE => get_string('bookreftype_conference', 'mod_bookref'),
                            BOOKREF_TYPE_ABSTRACT => get_string('bookreftype_abstract', 'mod_bookref'),
                            BOOKREF_TYPE_PREPRINT => get_string('bookreftype_preprint', 'mod_bookref'),
                            BOOKREF_TYPE_REPORT => get_string('bookreftype_report', 'mod_bookref'),
                            BOOKREF_TYPE_INTERNET => get_string('bookreftype_internet', 'mod_bookref')
                        ));
        $mform->addRule('type', null, 'required', null, 'client');

        $mform->addElement('text', 'publisher', get_string('bookref_publisher', 'bookref'), array('size' => '64'));
        $mform->setType('publisher', PARAM_RAW);
        $mform->addRule('publisher', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('text', 'address', get_string('bookref_address', 'bookref'), array('size' => '64'));
        $mform->setType('address', PARAM_RAW);
        $mform->addRule('address', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('text', 'year', get_string('bookref_year', 'bookref'), array('size' => '4'));
        $mform->setDefault('year', 2015);
        $mform->setType('year', PARAM_INT);
        $mform->addRule('year', null, 'numeric', null, 'client');

        $mform->addElement('text', 'pages', get_string('bookref_pages', 'bookref'), array('size' => '20'));
        $mform->setType('pages', PARAM_RAW);
        $mform->addRule('pages', get_string('maximumchars', '', 20), 'maxlength', 20, 'client');

        $mform->addElement('text', 'seriestitle', get_string('bookref_seriestitle', 'bookref'), array('size' => '64'));
        $mform->setType('seriestitle', PARAM_RAW);
        $mform->addRule('seriestitle', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $mform->addElement('text', 'number', get_string('bookref_number', 'bookref'), array('size' => '20'));
        $mform->setType('number', PARAM_RAW);
        $mform->addRule('number', get_string('maximumchars', '', 20), 'maxlength', 20, 'client');

        $mform->addElement('text', 'responsible', get_string('bookref_responsible', 'bookref'), array('size' => '64'));
        $mform->setType('responsible', PARAM_RAW);
        $mform->addRule('responsible', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('responsible', 'bookref_responsible', 'bookref');

        $mform->addElement('text', 'url', get_string('bookref_url', 'bookref'), array('size'=>'64'));
        $mform->setType('url', PARAM_RAW);

        $mform->addElement('date_selector', 'url_date', get_string('bookref_url_date', 'bookref'));
        $mform->setDefault('url_date', 0);

        $mform->addElement('filemanager', 'files', get_string('files'), null, array('subdirs'=>1, 'accepted_types'=>'*'));

        //-------------------------------------------------------
        $this->standard_coursemodule_elements();
        $this->add_action_buttons();
    }

    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        if (!empty($data['url'])) {
            $url = $data['url'];
            if (preg_match('|^/|', $url)) {
            } else if (preg_match('|^[a-z]+://|i', $url) or preg_match('|^https?:|i', $url) or preg_match('|^ftp:|i', $url)) {
                if (!url_appears_valid_url($url)) {
                    $errors['url'] = get_string('invalidurl', 'url');
                }
            } else if (preg_match('|^[a-z]+:|i', $url)) {

            } else {
                $errors['url'] = get_string('invalidurl', 'url');
            }
        }
        return $errors;
    }

    function data_preprocessing(&$default_values) {
        if ($this->current->instance) {
            // editing existing instance - copy existing files into draft area
            $draftitemid = file_get_submitted_draft_itemid('files');
            file_prepare_draft_area($draftitemid, $this->context->id, 'mod_bookref', 'content', 0, array('subdirs'=>true));
            $default_values['files'] = $draftitemid;
        }
    }
}
