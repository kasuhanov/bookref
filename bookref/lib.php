<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    mod_bookref
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

define('BOOKREF_LANG_RU', 0);
define('BOOKREF_LANG_EN', 1);

define('BOOKREF_TYPE_BOOK', 0);          //книга;
define('BOOKREF_TYPE_ARTICLE', 1);       //статья;
define('BOOKREF_TYPE_DISSERTATION', 2);  //диссертация;
define('BOOKREF_TYPE_CONFERENCE', 3);    //тезисы и конференции;
define('BOOKREF_TYPE_ABSTRACT', 4);      //автореферат;
define('BOOKREF_TYPE_PREPRINT', 5);      //препринт;
define('BOOKREF_TYPE_REPORT', 6);        //технический отчет:
define('BOOKREF_TYPE_INTERNET', 7);      //ссылка в Internet.

/**
 * Returns the information on whether the module supports a feature
 *
 * See {@link plugin_supports()} for more info.
 *
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed true if the feature is supported, null if unknown
 */
function bookref_supports($feature) {

    switch($feature) {
        case FEATURE_MOD_ARCHETYPE:     return MOD_ARCHETYPE_RESOURCE;
        case FEATURE_MOD_INTRO:         return false;
//      case FEATURE_SHOW_DESCRIPTION:  return true;
        default:                        return null;
    }
}

function bookref_normalize_string($str){
    if (substr(trim($str), -1) == '.') {
        return  substr($str, 0, -1);
    } else {
        return $str;
    }
}

/**
 * @param stdClass $bookref
 */
function bookref_setup_name($bookref){
    $author = bookref_normalize_string($bookref->author);
    $title = bookref_normalize_string($bookref->title);
    $publisher = bookref_normalize_string($bookref->publisher);
    $seriestitle = bookref_normalize_string($bookref->seriestitle);
    $responsible = bookref_normalize_string($bookref->responsible);
    $pages = bookref_normalize_string($bookref->pages);
    $number = bookref_normalize_string($bookref->number);
    $address = $bookref->address;
    $year = $bookref->year;
    $url = $bookref->url;
    $url_date = date("d.m.Y", $bookref->url_date);

    if($bookref->lang == BOOKREF_LANG_RU){
        $pg = get_string('bookref_page_ru', 'bookref');
    } else {
        $pg = get_string('bookref_page_en', 'bookref');
    }

    switch ($bookref->type){
        case BOOKREF_TYPE_BOOK:
            $bookref->name = "$author. $title. ";
            if(strlen($address) > 0){
                $bookref->name = $bookref->name."$address: ";
            }
            $bookref->name = $bookref->name."$publisher, $year. $pages $pg";
            break;
        case BOOKREF_TYPE_ARTICLE:
            $bookref->name = "$author. $title // $seriestitle. ";
            if(strlen($responsible) > 0){
                $bookref->name = $bookref->name."/ $responsible. ";
            }
            if(strlen($address) > 0){
                $bookref->name = $bookref->name."$address: ";
            }
            if(strlen($publisher) > 0){
                $bookref->name = $bookref->name."$publisher, ";
            }
            $bookref->name = $bookref->name."$year. ";
            if(strlen($number) > 0){
                $bookref->name = $bookref->name."$number. ";
            }
            $bookref->name = $bookref->name."$pages.";
            break;
        case BOOKREF_TYPE_DISSERTATION:
            $bookref->name = "$author. $title / $responsible. $address, $year. $pages.";
            break;
        case BOOKREF_TYPE_CONFERENCE:
            $bookref->name = "$author. $title // $seriestitle. $address: $publisher, $year. $pages.";
            break;
        case BOOKREF_TYPE_ABSTRACT:
            $bookref->name = "$author. $title / $responsible. $address, $year. $pages.";
            break;
        case BOOKREF_TYPE_PREPRINT:
            $bookref->name = "$author. $title. $address, $year $number.";
            break;
        case BOOKREF_TYPE_REPORT:
            $bookref->name = "$author. $title. $address: $publisher, $year.";
            break;
        case BOOKREF_TYPE_INTERNET:
            $bookref->name = "$author. $title: [$url], $url_date.";
//          12. Левин В.К. Отечественные суперкомпьютеры семейства МВС: [http://parallel.ru/mvs/levin.html], 27.05.2001.
            break;
    }
}

function bookref_setup_lang($bookref){
    $title = $bookref->title;
    if(preg_match('#[а-яё]+#i', $title)){
        $bookref->lang = BOOKREF_LANG_RU;
    } else {
        $bookref->lang = BOOKREF_LANG_EN;
    }
}

/**
 * Saves a new instance of the bookref into the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param stdClass $bookref Submitted data from the form in mod_form.php
 * @param mod_bookref_mod_form $mform The form instance itself (if needed)
 * @return int The id of the newly inserted bookref record
 */
function bookref_add_instance(stdClass $bookref, mod_bookref_mod_form $mform = null) {
    global $DB;

    $cmid        = $bookref->coursemodule;
    $draftitemid = $bookref->files;

    bookref_setup_lang($bookref);
    bookref_setup_name($bookref);

//    foreach (bookref_get_authors() as &$author) {
//        $bookref->name = $author;
//    }

    $bookref->timecreated = time();
    $bookref->id = $DB->insert_record('bookref', $bookref);

    foreach ($bookref->authors as &$author) {
        $bookref->name = $author;
        $DB->insert_record('bookref_author', array('bookref' => $bookref->id, 'author' => $author));
    }

    $DB->set_field('course_modules', 'instance', $bookref->id, array('id'=>$cmid));
    $context = context_module::instance($cmid);

    if ($draftitemid) {
        file_save_draft_area_files($draftitemid, $context->id, 'mod_bookref', 'content', 0, array('subdirs'=>true));
    }

    return $bookref->id;
}

/**
 * Updates an instance of the bookref in the database
 *
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param stdClass $bookref An object from the form in mod_form.php
 * @param mod_bookref_mod_form $mform The form instance itself (if needed)
 * @return boolean Success/Fail
 */
function bookref_update_instance(stdClass $bookref, mod_bookref_mod_form $mform = null){
    global $DB;

    $cmid = $bookref->coursemodule;
    $draftitemid = $bookref->files;

    $bookref->timemodified = time();
    $bookref->id = $bookref->instance;

    bookref_setup_lang($bookref);
    bookref_setup_name($bookref);

    $result = $DB->update_record('bookref', $bookref);

    $context = context_module::instance($cmid);
    if ($draftitemid = file_get_submitted_draft_itemid('files')) {
        file_save_draft_area_files($draftitemid, $context->id, 'mod_bookref', 'content', 0, array('subdirs'=>true));
    }

    return $result;
}

/**
 * This standard function will check all instances of this module
 * and make sure there are up-to-date events created for each of them.
 * If courseid = 0, then every bookref event in the site is checked, else
 * only bookref events belonging to the course specified are checked.
 * This is only required if the module is generating calendar events.
 *
 * @param int $courseid Course ID
 * @return bool
 */
function bookref_refresh_events($courseid = 0) {
    global $DB;

    if ($courseid == 0) {
        if (!$bookrefs = $DB->get_records('bookref')) {
            return true;
        }
    } else {
        if (!$bookrefs = $DB->get_records('bookref', array('course' => $courseid))) {
            return true;
        }
    }

    foreach ($bookrefs as $bookref) {
        // Create a function such as the one below to deal with updating calendar events.
        // bookref_update_events($bookref);
    }

    return true;
}

/**
 * Removes an instance of the bookref from the database
 *
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function bookref_delete_instance($id) {
    global $DB;

    if (! $bookref = $DB->get_record('bookref', array('id' => $id))) {
        return false;
    }

    // Delete any dependent records here.

    $DB->delete_records('bookref', array('id' => $bookref->id));
    $DB->delete_records('bookref_author', array('bookref' => $bookref->id));

    return true;
}

function bookref_get_authors() {
    global $DB;

    $sql = 'SELECT s.*
                  FROM { author } s
                  ';

    $persistents = [];

    $recordset = $DB->get_recordset_sql($sql);
    foreach ($recordset as $record) {
        $persistents[] = new static(0, $record);
    }
    $recordset->close();

    return $persistents;
}

/**
 * Returns a small object with summary information about what a
 * user has done with a given particular instance of this module
 * Used for user activity reports.
 *
 * $return->time = the time they did it
 * $return->info = a short text description
 *
 * @param stdClass $course The course record
 * @param stdClass $user The user record
 * @param cm_info|stdClass $mod The course module info object or record
 * @param stdClass $bookref The bookref instance record
 * @return stdClass|null
 */
function bookref_user_outline($course, $user, $mod, $bookref) {

    $return = new stdClass();
    $return->time = 0;
    $return->info = '';
    return $return;
}

/**
 * Prints a detailed representation of what a user has done with
 * a given particular instance of this module, for user activity reports.
 *
 * It is supposed to echo directly without returning a value.
 *
 * @param stdClass $course the current course record
 * @param stdClass $user the record of the user we are generating report for
 * @param cm_info $mod course module info
 * @param stdClass $bookref the module instance record
 */
function bookref_user_complete($course, $user, $mod, $bookref) {
}

/**
 * Given a course and a time, this module should find recent activity
 * that has occurred in bookref activities and print it out.
 *
 * @param stdClass $course The course record
 * @param bool $viewfullnames Should we display full names
 * @param int $timestart Print activity since this timestamp
 * @return boolean True if anything was printed, otherwise false
 */
function bookref_print_recent_activity($course, $viewfullnames, $timestart) {
    return false;
}

/**
 * Prepares the recent activity data
 *https://webauth.susu.ac.ru/login.html?redirect=www.gstatic.com/generate_204
 * This callback function is supposed to populate the passed array with
 * custom activity records. These records are then rendered into HTML via
 * {@link bookref_print_recent_mod_activity()}.
 *
 * Returns void, it adds items into $activities and increases $index.
 *
 * @param array $activities sequentially indexed array of objects with added 'cmid' property
 * @param int $index the index in the $activities to use for the next record
 * @param int $timestart append activity since this time
 * @param int $courseid the id of the course we produce the report for
 * @param int $cmid course module id
 * @param int $userid check for a particular user's activity only, defaults to 0 (all users)
 * @param int $groupid check for a particular group's activity only, defaults to 0 (all groups)
 */
function bookref_get_recent_mod_activity(&$activities, &$index, $timestart, $courseid, $cmid, $userid=0, $groupid=0) {
}

/**
 * Prints single activity item prepared by {@link bookrehttps://webauth.susu.ac.ru/login.html?redirect=www.gstatic.com/generate_204f_get_recent_mod_activity()}
 *
 * @param stdClass $activity activity record with added 'cmid' property
 * @param int $courseid the id of the course we produce the report for
 * @param bool $detail print detailed report
 * @param array $modnames as returned by {@link get_module_types_names()}
 * @param bool $viewfullnames display users' full names
 */
function bookref_print_recent_mod_activity($activity, $courseid, $detail, $modnames, $viewfullnames) {
}

/**
 * Function to be run periodically according to the moodle cron
 *
 * This function searches for things that need to be done, such
 * as sending out mail, toggling flags etc ...
 *
 * Note that this has been deprecated in favour of scheduled task API.
 *
 * @return boolean
 */
function bookref_cron () {
    return true;
}

/**
 * Returns all other caps used in the module
 *
 * For example, this could be array('moodle/site:accessallgroups') if the
 * module uses that capability.
 *
 * @return array
 */
function bookref_get_extra_capabilities() {
    return array();
}

/**
 * Serves the files from the bookref file areas
 *
 * @package mod_bookref
 * @category files
 *
 * @param stdClass $course the course object
 * @param stdClass $cm the course module object
 * @param stdClass $context the bookref's context
 * @param string $filearea the name of the file area
 * @param array $args extra arguments (itemid, path)
 * @param bool $forcedownload whether or not force download
 * @param array $options additional options affecting the file serving
 * @return bool
 */
function bookref_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG, $DB;

    if ($context->contextlevel != CONTEXT_MODULE) {
        return false;
    }

    require_course_login($course, true, $cm);
    if (!has_capability('mod/bookref:view', $context)) {
        return false;
    }

    if ($filearea !== 'content') {
        // intro is handled automatically in pluginfile.php
        return false;
    }

    array_shift($args); // ignore revision - designed to prevent caching problems only

    $fs = get_file_storage();
    $relativepath = implode('/', $args);
    $fullpath = "/$context->id/mod_bookref/content/0/$relativepath";
    if (!$file = $fs->get_file_by_hash(sha1($fullpath)) or $file->is_directory()) {
        return false;
    }

    // finally send the file
    // for folder module, we force download file all the time
    send_stored_file($file, 0, 0, true, $options);
}
/* Navigation API */

/**
 * Extends the global navigation tree by adding bookref nodes if there is a relevant content
 *
 * This can be called by an AJAX request so do not rely on $PAGE as it might not be set up properly.
 *
 * @param navigation_node $navref An object representing the navigation tree node of the bookref module instance
 * @param stdClass $course current course record
 * @param stdClass $module current bookref instance record
 * @param cm_info $cm course module information
 */
function bookref_extend_navigation(navigation_node $navref, stdClass $course, stdClass $module, cm_info $cm) {
    // TODO Delete this function and its docblock, or implement it.
}

/**
 * Extends the settings navigation with the bookref settings
 *
 * This function is called when the context for the page is a bookref module. This is not called by AJAX
 * so it is safe to rely on the $PAGE.
 *
 * @param settings_navigation $settingsnav complete settings navigation tree
 * @param navigation_node $bookrefnode bookref administration node
 */
function bookref_extend_settings_navigation(settings_navigation $settingsnav, navigation_node $bookrefnode=null) {
    // TODO Delete this function and its docblock, or implement it.
}//
function bookref_get_file_areas($course, $cm, $context) {
    $areas = array();
    $areas['content'] = get_string('foldercontent', 'folder');

    return $areas;
}

function bookref_get_file_info($browser, $areas, $course, $cm, $context, $filearea, $itemid, $filepath, $filename) {
    global $CFG;


    if ($filearea === 'content') {
        if (!has_capability('mod/bookref:view', $context)) {
            return NULL;
        }
        $fs = get_file_storage();

        $filepath = is_null($filepath) ? '/' : $filepath;
        $filename = is_null($filename) ? '.' : $filename;
        if (!$storedfile = $fs->get_file($context->id, 'mod_bookref', 'content', 0, $filepath, $filename)) {
            if ($filepath === '/' and $filename === '.') {
                $storedfile = new virtual_root_file($context->id, 'mod_bookref', 'content', 0);
            } else {
                // not found
                return null;
            }
        }

        require_once("$CFG->dirroot/mod/bookref/locallib.php");
        $urlbase = $CFG->wwwroot.'/pluginfile.php';

        // students may read files here
        $canwrite = has_capability('mod/bookref:managefiles', $context);
        return new bookref_content_file_info($browser, $context, $storedfile, $urlbase, $areas[$filearea], true, true, $canwrite, false);
    }

    // note: folder_intro handled in file_browser automatically

    return null;
}

/*
function bookref_cm_info_view(cm_info $cm) {
    global $PAGE;
    if ($cm->uservisible && //$cm->customdata &&
        has_capability('mod/bookref:view', $cm->context)) {
        $bookref = new stdClass();
        $bookref->id = (int)$cm->instance;
        $bookref->course = (int)$cm->course;
        $bookref->name = $cm->name;
        $renderer = $PAGE->get_renderer('mod_bookref');
        $cm->set_content($renderer->display_bookref($bookref));
    }
}*/
