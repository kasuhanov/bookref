<?php


$string['modulename'] = 'Теория';
$string['modulenameplural'] = 'Теории';
$string['modulename_help'] = 'Use the theory module for... | The theory module allows...';
$string['theory:addinstance'] = 'Добавить новую теорию';
$string['theory:submit'] = 'Сохранить теорию';
$string['theory:view'] = 'View theory';
$string['theoryfieldset'] = 'Custom example fieldset';
$string['theory_name'] = 'Заголовок';
$string['theory_content'] = 'Содержимое';
$string['theory'] = 'theory';
$string['notheorys'] = 'notheorys';
$string['pluginadministration'] = 'theory administration';
$string['pluginname'] = 'Теория';
$string['type'] = 'Тип';
