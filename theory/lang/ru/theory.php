<?php


$string['modulename'] = 'Теория';
$string['modulenameplural'] = 'Теории';
$string['modulename_help'] = 'Теория — это структурированный набор текстовой, табличной и графической информации.
 
 Данный элемент состоит из заголовка и содержимого.
  В качестве содержимого могут выступать форматированный текст, таблицы, изображения, формулы и файлы.
   Также содержимое поддерживает нумерованные и ненумерованные списки.';
$string['theory:addinstance'] = 'Добавить новую теорию';
$string['theory:submit'] = 'Сохранить теорию';
$string['theory:view'] = 'View theory';
$string['theoryfieldset'] = 'Custom example fieldset';
$string['theory_name'] = 'Заголовок';
$string['theory_content'] = 'Содержимое';
$string['theory'] = 'theory';
$string['notheorys'] = 'notheorys';
$string['pluginadministration'] = 'theory administration';
$string['pluginname'] = 'Теория';
$string['type'] = 'Тип';
