<?php

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_module ID, or
$n  = optional_param('n', 0, PARAM_INT);  // ... theory instance ID - it should be named as the first character of the module.

if ($id) {
    $cm         = get_coursemodule_from_id('theory', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $theory  = $DB->get_record('theory', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $theory  = $DB->get_record('theory', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $theory->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('theory', $theory->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);

$PAGE->set_url('/mod/theory/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($theory->name));
$PAGE->set_heading(format_string($course->fullname));

echo $OUTPUT->header();
echo $OUTPUT->heading($theory->name);

if ($theory->intro) {
    echo $OUTPUT->box(format_module_intro('theory', $theory, $cm->id), 'generalbox mod_introbox', 'theoryintro');
}

echo $OUTPUT->footer();
