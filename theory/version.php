<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2016052300;
$plugin->requires  = 2016051900;
$plugin->component = 'mod_theory';
$plugin->cron      = 0;